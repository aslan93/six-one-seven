<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'layout' => 'frontend',
    'language' => 'ro',
    'defaultRoute' => 'home',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'asdasdasdasdasdasdasdasdasdas',
        ],
        'response' => [
            'formatters' => [
                'pdf' => [
                    'class' => 'robregonm\pdf\PdfResponseFormatter',
                ]
            ]
        ],
        'user' => [
            'identityClass' => 'app\modules\User\models\User',
            'enableAutoLogin' => true,
        ],
        'mobileDetect' => [
            'class' => '\skeeks\yii2\mobiledetect\MobileDetect'
        ],
        'assetManager' => [
            'forceCopy' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/views/themes/shop617',
                'baseUrl' => '@web/views/themes/shop617',
                'pathMap' => [
                    '@app/views' => '@app/views/themes/shop617',
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'languages' => array_keys($params['lang']),
            'enableLanguageDetection' => false,
            'enableDefaultLanguageUrlCode' => true,
            'rules' => [
                'contacts' => 'info/contacts',
                'about' => 'info/about',
                'cart' => 'admin/ecommerce/cart/cart/view/',
                'facture' => 'admin/ecommerce/cart/cart/facture/',
                'payment' => 'admin/ecommerce/cart/cart/payment/',
                'finish' => 'admin/ecommerce/cart/cart/finish/',
                'downloadPDF' => 'admin/ecommerce/cart/cart/view/',
                'login' => '/login/login/',
                'account' => '/login/login/view',
                'account/logout' => '/login/login/logout',
                'account/order' => 'login/login/order',
                '' => 'home',
            ]
        ],
    ],

    'modules' => [
        'admin' => [
            'class' => 'app\modules\Admin\Admin',
            'defaultRoute' => 'default/index',
            'modules' => [
                'ecommerce' => [
                    'class' => 'app\modules\Ecommerce\Ecommerce',
                    'modules' => [
                        'product' => [
                            'class' => 'app\modules\Product\Product',
                            'defaultRoute' => 'product/index',
                        ],
                        'category' => [
                            'class' => 'app\modules\Category\Category',
                            'defaultRoute' => 'category/index',
                        ],
                        'filter' => [
                            'class' => 'app\modules\Filter\Filter',
                            'defaultRoute' => 'filter/index',
                        ],
                        'cart' => [
                            'class' => 'app\modules\Cart\Cart',
                            'defaultRoute' => 'cart/index',
                        ],
                    ],
                ],
                'user' => [
                    'class' => 'app\modules\User\User',
                    'defaultRoute' => 'user/index',
                ],
                'post' => [
                    'class' => 'app\modules\Post\Post',
                    'defaultRoute' => 'post/index',
                ],
                'feedback' => [
                    'class' => 'app\modules\Feedback\Feedback',
                    'defaultRoute' => 'feedback/index',
                ],
                'newsletter' => [
                    'class' => 'app\modules\Newsletter\Newsletter',
                    'defaultRoute' => 'newsletter/index',
                ],
                'settings' => [
                    'class' => 'app\modules\Settings\Settings',
                    'defaultRoute' => 'default/settings',
                ],
             ],
        ],
        'login' => [
            'class' => 'app\modules\Login\Login',
            'defaultRoute' => 'login/index',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
