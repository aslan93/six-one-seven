<?php

namespace app\controllers;


use app\modules\Newsletter\models\Newsletter;
use Yii;
use app\modules\Product\models\Product;
use app\modules\Post\models\Post;
use yii\caching\TagDependency;


class HomeController extends FrontController
{
    public function actionIndex()
    {
        $newProducts = Product::getDb()->cache(function ($db) {
            return Product::find()->where(['MarkNew'=>1])->with('lang','mainImage')->limit(4)->all();
        }, 3600, new TagDependency(['tags' => Product::className()]));
        $saleProducts = Product::getDb()->cache(function ($db) {
            return Product::find()->where(['MarkSell'=>1])->with('lang','mainImage')->limit(4)->all();
        }, 3600, new TagDependency(['tags' => Product::className()]));
        $news = Post::getDb()->cache(function ($db) {
            return Post::find()->with('lang','mainImage')->orderBy('CreatedAt Desc')->limit(3)->all();
        }, 3600, new TagDependency(['tags' => Post::className()]));
        $newsletter = new Newsletter();

        return $this->render('index',[
            'newProducts' => $newProducts,
            'saleProducts' => $saleProducts,
            'news' => $news,
            'newsletter' => $newsletter,
        ]);
    }
}

