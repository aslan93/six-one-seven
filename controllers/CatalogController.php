<?php

namespace app\controllers;

use app\modules\Attribute\models\Attribute;
use app\modules\Category\models\Category;
use app\modules\Category\models\CategoryLang;
use app\modules\Filter\models\ProductFilterValue;
use app\modules\Post\models\PostLang;
use app\modules\Product\models\Product;
use app\modules\Product\models\ProductLang;
use app\modules\Product\models\ProductSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\base\Theme;
use app\modules\Post\models\Post;
use yii\data\ActiveDataProvider;
use app\modules\Product\models\FrontProductSearch;
use yii\caching\TagDependency;

class CatalogController extends FrontController
{


    public function actionIndex()
    {
        $searchModel = new FrontProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'searchModel'=> $searchModel,
        ]);
    }

    public function actionProduct($slug)
    {
        $product = Product::find()->joinWith('lang')->where(['ProductLang.Slug'=>$slug])->one();
        if (!$product){
            return $this->redirect('/catalog');
        }else {
            $productFilters = $product->filters;
            $productColors = $product->productColors;
            $recomandations = Product::find()->with('lang','mainImage')->where(['CategoryID' => $product->CategoryID])->andWhere(['<>', 'ID', $product->ID])->limit(2)->all();
            $specifications = explode('<p>', $product->lang->Specifications);
            unset($specifications[0]);
            $caracteristics = explode('<p>', $product->lang->Caracteristics);
            unset($caracteristics[0]);

            return $this->render('singleProduct', [
                'product' => $product,
                'recomandations' => $recomandations,
                'specifications' => $specifications,
                'caracteristics' => $caracteristics,
                'productColors' => $productColors,
                'productFilters' => $productFilters,
            ]);
        }
    }
    public function actionCategory($cid)
    {
        $category = Category::find()->where(['ID'=>$cid])->with('lang')->one();
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find()->where(['CategoryID'=>$cid])->with('lang','mainImage'),
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);
        return $this->render('category',[
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionColorImage($cid){
        $pfv = ProductFilterValue::findOne($cid);
        return $pfv->pFVImage->productImage;
    }

    public function actionResult($slug)
    {   if(ProductLang::find()->where(['slug'=>$slug])->one()){
        return $this->redirect(['/catalog/product','slug'=>$slug]);
        }elseif($post = PostLang::find()->where(['Title'=>$slug])->one()) {
            return $this->redirect(['/blog/post','id'=> $post->ArticleID]);
        }elseif($slug){
            $category = CategoryLang::find()->where(['Title' => $slug])->one();
            return $this->redirect(['/catalog/category','cid'=>$category->CategoryID]);
        }else{
            return $this->render('noResults');
        }
    }



}