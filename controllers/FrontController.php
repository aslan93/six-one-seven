<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\base\Theme;
use app\modules\Settings\models\Settings;
use yii\caching\TagDependency;

class FrontController extends Controller
{

    public function init()
    {
        parent::init();

        Yii::$container->set('yii\widgets\Pjax', ['timeout' => 5000]);

        $settings = Settings::getDb()->cache(function ($db) {
            return Settings::find()->indexBy('Name')->all();
        }, 3600, new TagDependency(['tags' => 'Settings']));

        foreach ($settings as $settingName => $settingsItem)
        {
            Yii::$app->params['settings'][$settingName] = $settingsItem->Value;
        }

        $this->layout = 'frontend';
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}