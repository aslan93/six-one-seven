<?php

namespace app\controllers;


use app\modules\Cart\controllers\QHelper;
use Yii;
use app\modules\Product\models\Product;
use app\modules\Post\models\Post;
use app\modules\Newsletter\models\Newsletter;


class AjaxController extends FrontController
{


    public function actionSearch($term)
    {
        $data1 = \app\modules\Product\models\ProductLang::find()
            ->select(["Name as value" ,"Name as label"  , "Slug as id"])
            ->where(['like','Name',$term])
            ->andWhere(['LangID' => Yii::$app->language])
            ->asArray()
            ->all();

        $dataBlog = \app\modules\Post\models\PostLang::find()
            ->select(["Title as value" ,"Title as label"  , "Title as id"])
            ->where(['like','Title',$term])
            ->andWhere(['LangID' => Yii::$app->language])
            ->asArray()
            ->all();

        $dataCategory = \app\modules\Category\models\CategoryLang::find()
            ->select(["Title as value" ,"Title as label"  , "Title as id",])
            ->where(['like','Title',$term])
            ->andWhere(['LangID' => Yii::$app->language])
            ->asArray()
            ->all();

        $data = array_merge($data1,$dataBlog,$dataCategory);

        return json_encode($data);
    }

    public function actionAddNewsletter()
    {
        $model = new Newsletter();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return '<div class="small-title" style="margin-left: 200px;"><h3>Abonat cu success</h3></div>';
        } else {
            return '<div class="small-title" style="margin-left: 200px;" ><span class="text-danger"><h3>Deja Abonat</h3></span></div>';
        }
    }
}