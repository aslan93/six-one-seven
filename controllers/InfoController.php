<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\base\Theme;

class InfoController extends FrontController
{

    public function actionContacts()
    {
        return $this->render('contacts');
    }
    public function actionAbout()
    {
        return $this->render('about');
    }
}