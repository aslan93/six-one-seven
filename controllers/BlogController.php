<?php

namespace app\controllers;

use app\modules\Post\models\Post;
use Yii;
use app\modules\Post\models\PostSearch;


class BlogController extends FrontController
{


    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionPost($id)
    {
        $post = Post::find()->where(['ID'=>$id])->with('lang')->one();
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('singlePost', [
            'post' => $post,
            'dataProvider' => $dataProvider,
        ]);
    }
}