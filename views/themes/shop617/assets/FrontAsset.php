<?php

namespace app\views\themes\shop617\assets;

use yii\web\AssetBundle;

class FrontAsset extends AssetBundle
{
    public $sourcePath = '@app/views/themes/shop617/assets/files';

    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/animate.css',
        'css/swiper.min.css',
        'css/jquery.formstyler.css',
        'css/select2.min.css',
        'css/main.css',
    ];

    public $js = [
  //  'js/jquery.js',
    'js/jquery.matchHeight-min.js',
    'js/bootstrap.min.js',
    'js/jquery.matchHeight-min.js',
    'js/jquery.scrollme.min.js',
    'js/jquery.mousewheel.min.js',
    'js/swiper.js',
    //'js/select2.min.js',
    'js/jquery.formstyler.min.js',
    'js/scripts.js',
    ];


    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\select2\Select2Asset',

    ];
}