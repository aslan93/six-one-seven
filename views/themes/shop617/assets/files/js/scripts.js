
/* MAPER GOOGLE MAP */

var Maper = {
    contacts_pageMap: function () {
        var map1 = new google.maps.Map(document.getElementById('map-1'), {
            zoom: 13,
            center: {lat: 47.0254829, lng: 28.8281552},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false,
            styles: [
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 40
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                }
            ]
        });

        var image = 'images/placeholder.png';
        var beachMarker = new google.maps.Marker({
            position: {lat: 47.0254829, lng: 28.8281552},
            map: map1,
            icon: image
        });
    }
};

$(document).ready(function(){

    $("nav ul li .catalog").click(function(e){
        e.preventDefault();
        $("nav ul li .catalog").removeClass("open").css({color: "#fff"});
        $(".submenu").not($(this).addClass("open").css({color: "#ff5000" , "text-decoration" : "none"}).closest("li").find(".submenu").fadeIn(300)).fadeOut(300);
    });

    $("body section").click(function(e){
        $(".submenu").not($(this).removeClass("open").closest("li").find(".submenu")).fadeOut(300);
    });


    var swiper = new Swiper('.parteners-slider', {
        slidesPerView: 3,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 20,
        nextButton: '.parteners-next-btn',
        prevButton: '.parteners-prev-btn'
    });

   // $(".style-select").select2();


    $("input[type='checkbox']").styler();



    /* PRODUCTS SLIDER */
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;



    $(".toggle-search").click(function(e){
        e.preventDefault();
        $(".dropdown-search").slideToggle(300);
    });

    $(".toggle-menu").click(function(e){
        e.preventDefault();
       $(this).closest("header").find(".bottom-header").slideToggle(300);
    });

    $(".toggle-sidebar").click(function(e){
        e.preventDefault();
        $(this).closest(".left-slider-filter").find("form").slideToggle(300);
    });


    $("#back-to-top").click( function() {
        $("html , body").animate({
            scrollTop: 0
        } , $(window).scrollTop() / 5 , "swing" )
    });

    $(window).scroll(function(){
        if($(this).scrollTop() > 500){
            $("#back-to-top").fadeIn(400);
        }
        else{
            $("#back-to-top").fadeOut(400);
        }
    });


    if($(window).width() < 992){

        $(document).on('click','.shop-cart > a',function(){
            $(".mobile-cart").slideToggle();
            $("html , body").addClass("disabled-scroll");
        });
        $(document).on('click','.close-shop-cart',function(){
            $(".dropdown-shop-cart").slideUp();
            $("html , body").removeClass("disabled-scroll");
        });
    }
    else{
        $(document).on('click','.shop-cart > a',function(){
            $(".dropdown-shop-cart").slideToggle();
        });

        $(document).on('click','.close-shop-cart',function(){
            $(".dropdown-shop-cart").slideUp();
        });
    }



    $(document).on('click','.remove-product-cart',function(){

        $.get( baseUrl('/admin/ecommerce/cart/cart/delete-item/?md5id=') + $(this).attr('data-id')
            ).success( function (data) {
            $.pjax.reload({container: '#cart-info-pjax'});
            $.pjax.reload({container: '#cart-info-pjaxm'});
        });
        $(this).closest(".products-added").remove();
    });


    $(document).on('click','.remove-product',function(){

        $.get( baseUrl('/admin/ecommerce/cart/cart/delete-item/?md5id=')+ $(this).attr('data-id')
        ).success( function (data) {
            $.pjax.reload({container: '#p1'});
        });
        $(this).closest(".product").remove();
    });



    $(document).on('click','.btn-number',function(e){
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[id='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled');
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled');
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });



});











