<?php

use app\views\themes\shop617\assets\FrontAsset;

use yii\widgets\Menu;
use yii\bootstrap\Nav;
use app\modules\Cart\widgets\CartInfo\CartInfo;
use app\modules\Cart\widgets\CartInfoMobile\CartInfoMobile;
use kartik\growl\Growl;
use app\modules\Settings\Settings;
use yii\jui\AutoComplete;
use app\modules\Product\models\Product;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\modules\Category\models\Category;
use yii\caching\TagDependency;
?>
<?php
$bundle = FrontAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=0" />
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700|Roboto:400,700&amp;subset=cyrillic,cyrillic-ext,greek-ext,latin-ext" rel="stylesheet">
    <title><?= $this->title?></title>
    <script>function baseUrl(url){ return '<?=\yii\helpers\Url::to(['/'])?>' + url }</script>
    <?php $this->head() ?>

    <!-- Redirect to http://browsehappy.com/ on IE 8-  -->
    <!--[if lte IE 8]>

    <style type="text/css">
        body{display:none!important;}
    </style>

    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/" />

    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
<div class="dropdown-shop-cart mobile-cart hidden-lg hidden-md hidden-sm">
    <div class="row fixed-mobile">
        <div class="col-md-10 col-sm-10 col-xs-10">
            <div class="name-box">
                Cosul cu produse
            </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <div class="close-shop-cart">
                <span class="fa fa-close"></span>
            </div>
        </div>
    </div>
    <div class="separator-border"></div>
    <?php
    if(\skeeks\yii2\mobiledetect\MobileDetect::getInstance()->isMobile()){
        echo CartInfoMobile::widget();
    }
    ?>

    <div class="separator-border"></div>
    <a href="/cart" class="open-shop-cart-page mt20">
        Vezi cosul & Checkout
    </a>
</div>
<?php
if (Yii::$app->session->getFlash('success'))
{
    echo Growl::widget([
        'type' => Growl::TYPE_SUCCESS,
        'icon' => 'glyphicon glyphicon-ok-sign',
        'title' => Yii::t('app', 'Success'),
        'showSeparator' => true,
        'body' => Yii::$app->session->getFlash('success')
    ]);
}
?>



<header>
    <div class="top-header hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="call-now">
                        AVETI NEVOIE DE AJUTOR? SUNATI-NE
                        <a href="#"><?=Settings::getByName('phone')?></a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="user-elem">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <?php
                                if (yii::$app->user->isGuest){
                                    ?>
                                    <a href="<?=Url::to(['/login'])?>">
                                        LOGARE/INREGISTRARE
                                    </a>
                                    <?php
                                }else {
                                    ?>
                                    <a href="<?=Url::to(['/account'])?>">
                                        CONTUL MEU
                                    </a>
                                    /
                                    <a href="<?=Url::to(['/account/logout'])?>" class="">
                                        LOGOUT
                                    </a>
                                    <?php
                                }
                                    ?>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <?php
                                if(!\skeeks\yii2\mobiledetect\MobileDetect::getInstance()->isMobile()){
                                    echo CartInfo::widget();
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="middle-header">
        <div class="container">
            <div class="row hidden-xs">
                <div class="col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 text-left">
                            <div class="logo">
                                <a href="/">
                                    <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <div class="slogan">
                                Six One Seven <br />
                                <span class="small">
                                    Lorem ipsum dolor sit amet
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="delivery">
                        <a href="#">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 text-right">
                                    <span>
                                        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/package2.png" alt="">
                                    </span>
                                </div>
                                <div class="col-md-8 col-sm-8 text-left">
                                     <span>
                                        LIVRARE GRATUITA <br />
                                        IN ROMANIA
                                     </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">

                    <div class="search">
                        <form action="/catalog/result/" method="get">
                            <label>
                                <?=  AutoComplete::widget([
                                    'options'=>[
                                        'placeholder'=>'Cautare',
                                    ],
                                    'clientOptions' => [
                                        'source' => \yii\helpers\Url::to(['/ajax/search/']),
                                        'autoFill'=>true,
                                        'minLength'=>'4',
                                        'select' => new JsExpression("function( event, ui ) {
                                                    $('#search-slug').val(ui.item.id);
                                                                     }")
                                    ],
                                ]);

                                ?>
                                <div class="hidden">
                                    <input type="text" id="search-slug" name="slug">
                                    <!--                                    <input type="text" id="search-slug" name="type">-->
                                </div>

                            </label>
                            <button type="submit" class="btn-default">
                                <span class="fa fa-search"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row hidden-lg hidden-sm text-center">
                <div class="col-xs-4 hidden-lg hidden-md hidden-sm">
                    <div class="shop-cart">
                        <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-4 hidden-lg hidden-md hidden-sm">
                    <div class="logo-middle">
                        <a href="#">
                            <img src="<?=$bundle->baseUrl?>/images/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-xs-4 hidden-lg hidden-md hidden-sm">
                    <a href="#" class="toggle-menu">
                        <span class="fa fa-navicon"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-header">
        <div class="container">
            <nav>
                <ul>
                    <li>
                        <a href="/home">
                            ACASA
                        </a>
                    </li>
                    <li>
                        <a href="/about">
                            DESPRE 617
                        </a>
                    </li>
                    <li>
                        <a href="/catalog">
                            CATALOG
                        </a>
                    </li>
                    <li>
                        <a href="#" class="catalog">
                            CATEGORII
                        </a>
                        <div class="submenu">
                            <div class="row">
                                <div class="col-md-5 hidden-sm hidden-xs">
                                    <div class="image">
                                        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/drop-down-menu-image.png" alt="">
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-12">
                                    <div class="submenu-nav">
                                        <div class="row">
                                            <?php
                                            $categoriesArr = Category::getDb()->cache(function ($db) {
                                                return Category::find()->with('lang')->all();
                                            }, 3600, new TagDependency(['tags' => Category::className()]));
                                            $categoriesArr = array_chunk($categoriesArr,6);
                                            ?>
                                            <?php
                                            foreach ($categoriesArr as $categories){
                                                ?>
                                            <div class="col-md-4 col-sm-4">
                                                <ul>
                                                    <?php
                                                    foreach ($categories as $category){
                                                        ?>
                                                        <li>
                                                            <a href="<?=\yii\helpers\Url::to(['/catalog/category','cid'=> $category->ID])?>">
                                                                <?=$category->lang->Title?>
                                                            </a>
                                                        </li>

                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                                <?php
                                            }
                                            ?>






                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href="/contacts">
                            CONTACTE
                        </a>
                    </li>
                    <li>
                        <a href="/blog">
                            BLOG
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>

</header>
<?= $content ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-sm-12 col-12">
                <div class="logo">
                    <a href="/">
                        <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/footer-logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="text">
                    AVETI NEVOIE DE AJUTOR? SUNATI-NE <a href="#"><?=Settings::getByName('phone')?></a> <br />
                    <div class="copyright">
                        Copyright 617 &copy; 2017 by <a href="#">NIXAP</a>
                    </div>
                </div>
            </div>
            <div class="col-md-7  hidden-sm hidden-xs">
                <nav>
                    <ul>

                        <li>
                            <a href="/about">
                                DESPRE 617
                            </a>
                        </li>
                        <li>
                            <a href="/catalog">
                                CATALOG
                            </a>
                        </li>
                        <li>
                            <a href="/contacts">
                                CONTACTE
                            </a>
                        </li>
                        <li>
                            <a href="/blog">
                                BLOG
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</footer>


<a href="#" id="back-to-top">
    <span class="fa fa-chevron-circle-up"></span>
</a>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>