<?php

use app\views\themes\shop617\assets\FrontAsset;
use yii\widgets\ListView;

$bundle = FrontAsset::register($this);
?>

<section class="name-page">
    <div class="container">
        <div class="big-title">
            Blog
        </div>
    </div>
</section>

<section class="blog different">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Acasa</a></li>
            <li class="breadcrumb-item active">BLOG</li>
        </ol>
        <div class="row">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{summary}\n{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                'itemView' => function ($model, $key,$index) {
                    if ($index % 2 == 0) {
                        return $this->render('_post_item_up', ['model' => $model]);
                    } else {
                        return $this->render('_post_item_down', ['model' => $model]);
                    }
                }
            ]);
            ?>


        </div>
<!--        <div class="text-right">-->
<!--            <nav aria-label="Page navigation">-->
<!--                <ul class="pagination">-->
<!--                    <li>-->
<!--                        <a href="#" aria-label="Previous">-->
<!--                            <span aria-hidden="true">&laquo;</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="active"><a href="#">1</a></li>-->
<!--                    <li><a href="#">2</a></li>-->
<!--                    <li><a href="#">3</a></li>-->
<!--                    <li><a href="#">4</a></li>-->
<!--                    <li><a href="#">5</a></li>-->
<!--                    <li>-->
<!--                        <a href="#" aria-label="Next">-->
<!--                            <span aria-hidden="true">&raquo;</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </nav>-->
<!--        </div>-->
    </div>
</section>

