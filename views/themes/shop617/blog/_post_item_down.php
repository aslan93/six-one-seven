<?php
use yii\bootstrap\Html;

?>
<div class="col-md-4" data-mh="11">
    <div class="blog-box">
        <a href="<?=\yii\helpers\Url::to(['/blog/post','id'=>$model->ID])?>">

            <div class="info">
                <div class="small-title">
                    <?=$model->lang->Title?>
                </div>
                <div class="date-post">
                    <?=$model->niceDate?>
                </div>
                <div class="description">
                    <?=$model->shortContent?>
                </div>
            </div>
            <div class="img">
                <?= Html::img($model->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
            </div>
        </a>
    </div>
</div>