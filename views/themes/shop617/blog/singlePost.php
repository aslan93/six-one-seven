<?php
use app\views\themes\shop617\assets\FrontAsset;
use yii\helpers\Html;
use yii\widgets\ListView;

$bundle = FrontAsset::register($this);

?>

<section class="name-page">
    <div class="container">
        <div class="big-title">
            Blog
        </div>
    </div>
</section>

<section class="post">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Acasa</a></li>
            <li class="breadcrumb-item"><a href="/blog">Blog</a></li>
            <li class="breadcrumb-item active"><?=$post->lang->Title?></li>
        </ol>
        <div class="about-post">
            <div class="row">
                <div class="col-md-5">
                    <div class="img-box">
                        <?= Html::img($post->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                    </div>
                </div>
                <div class="info-product">
                    <div class="big-different-title">
                        <?=$post->lang->Title?>
                    </div>
                </div>
                    <?=$post->lang->Content?>

            </div>
        </div>
    </div>
</section>

<section class="blog different">
    <div class="container">
        <div class="text-left">
            <div class="title">
                Alte bloguri
            </div>
        </div>
        <div class="row">
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                'itemView' => function ($model, $key,$index) {
                    if ($index % 2 == 0) {
                        return $this->render('_post_item_up', ['model' => $model]);
                    } else {
                        return $this->render('_post_item_down', ['model' => $model]);
                    }
                }
            ]);
            ?>
        </div>
    </div>
</section>
