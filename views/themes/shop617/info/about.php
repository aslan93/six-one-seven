<?php

use app\views\themes\shop617\assets\FrontAsset;
use app\modules\Settings\Settings;

$bundle = FrontAsset::register($this);
?>


<section class="name-page">
    <div class="container">
        <div class="big-title">
            Despre noi
        </div>
    </div>
</section>

<section class="about-us different">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Acasa</a></li>
            <li class="breadcrumb-item active">DESPRE NOI</li>
        </ol>
        <div class="row">
            <div class="col-md-5">
                <div class="image-box-about-us" data-mh="5">
                    <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/middle-presentation-bg.png" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="description-company" data-mh="5">
                    <div class="small-title">
                        Despre 617
                    </div>
                    <div class="description">
                        <div class="mt15">
                            <?=Settings::getByName('aboutUS',true)?>
                        </div>
                    </div>
                    <a href="/contacts" class="see-more contact-us">
                        CONTACTATI-NE
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="parteners-and-payout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="payout-group" data-mh="8">
                    <div class="small-title">
                        Metoda <br />
                        de plata
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/pay-pal.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/visa.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/master-card.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/western-union.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/maestro-card.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="parteners" data-mh="8">
                    <div class="small-title">
                        Partenerii <br />
                        nostri
                    </div>
                    <div class="swiper-container parteners-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                        </div>
                        <!-- Add BUTTON NAV -->
                        <div class="swiper-button-next parteners-next-btn">
                            <img src="<?=$bundle->baseUrl?>/images/slider-next.png" alt="">
                        </div>
                        <div class="swiper-button-prev parteners-prev-btn">
                            <img src="<?=$bundle->baseUrl?>/images/slider-prev.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

