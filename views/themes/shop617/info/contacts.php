<?php

use app\views\themes\shop617\assets\FrontAsset;
use yii\web\View;
use app\modules\Settings\Settings;
$bundle = FrontAsset::register($this);
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5pXp7Lol5kF9oPCrWOukTc5k3mNImjwI" type="text/javascript"></script>
<section>
<div class="map" id="map-1">
    <div class="container">
        <div class="big-title">
            Contacte
        </div>
    </div>
</div>

<div class="contacts">
    <div class="container">
        <div class="contacts-group">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="contact-box" data-mh="11">
                        <div class="icon-box">
                            <span class="fa fa-map-pin"></span>
                        </div>
                        <div class="name-contact" data-mh="12">
                            Adresa noastra
                        </div>
                        <div class="text">
                            <?=Settings::getByName('address')?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="contact-box">
                        <div class="icon-box">
                            <a href="#">
                                <span class="fa fa-phone"></span>
                                <?=Settings::getByName('phone')?>
                            </a>
                        </div>
                        <div class="name-contact" data-mh="12">
                            <hr>
                        </div>
                        <div class="text">
                            <a href="#">
                                <span class="fa fa-envelope"></span>
                                <?=Settings::getByName('email')?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="contact-box" data-mh="11">
                        <div class="icon-box">
                            <span class="fa fa-clock-o"></span>
                        </div>
                        <div class="name-contact" data-mh="12">
                            Orele de lucru
                        </div>
                        <div class="text">
                            <?=Settings::getByName('grafic',true)?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-us-now text-center">
            <div class="big-different-title">
                Lasati un mesaj
            </div>
            <?=\app\modules\Feedback\components\FeedbackWidget\FeedbackWidget::widget()?>
        </div>
    </div>
</div>
</section>
<?php
$this->registerJs('Maper.contacts_pageMap();',View::POS_LOAD );
?>
