<?php

use app\views\themes\shop617\assets\FrontAsset;
use yii\bootstrap\Html;
use app\modules\Settings\Settings;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$bundle = FrontAsset::register($this);
?>

<style>
    #newsletter-email{
        display: inline-block;
        width: 80%!important;
        height: 52px;
        background-color: #ffffff;
        border: solid 1px #ebebeb;
        padding: 0 0 0 20px;
        font-family: "United Sans Rg Light" , sans-serif;
        font-size: 15px;
        text-align: left;
        color: #000;
    }

    .newsletter-btn{
        display: inline-block;
        /* max-width: 300px; */
        /* width: 100%; */
        padding: 18px 18px;
        background-color: #ff5000;
        text-align: center;
        -webkit-transition: all 0.3s ease-in;
        -moz-transition: all 0.3s ease-in;
        -ms-transition: all 0.3s ease-in;
        -o-transition: all 0.3s ease-in;
        transition: all 0.3s ease-in;
        font-family: "United Sans Rg Black" , sans-serif;
        font-size: 15px;
        font-weight: 900;
        line-height: 0.93;
        color: #ffffff;
    }
</style>

<section class="presentation top">
    <div class="container">
        <div class="presentation-elem">
            <div class="content">
                <div class="big-title">
                    Aliquam tincidunt <br />
                    mauris eu risus.
                </div>
                <div class="short-description">
                    Morbi in sem quis dui placerat ornare. Pellentesque odio nisi,
                    euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras
                    consequat. Praesent dapibus, neque id cursus faucibus, tortor
                    neque egestas augue, eu vulputate magna eros eu erat.
                    Aliquam erat volutpat. Nam dui mi, tincidunt quis,
                    accumsan porttitor, facilisis luctus, metus.
                </div>
                <div class="text-center">
                    <a href="<?=\yii\helpers\Url::to(['info/about'])?>">
                        MAI MULTE DETALII
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="facility">
    <div class="container">
        <div class="facility-group">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="facility-box" data-mh="1">
                        <div class="small-title" data-mh="2">
                            Vivamus <br />
                            vestibulum
                        </div>
                        <div class="icon-box">
                            <img src="<?=$bundle->baseUrl?>/images/package.png" alt="">
                        </div>
                        <div class="short-description">
                            Morbi in sem quis dui placerat ornare. Pellentesque
                            odio nisi, euismod in, pharetra a, ultricies in, diam.
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="facility-box" data-mh="1">
                        <div class="small-title" data-mh="2">
                            Vivamus <br />
                            vestibulum
                        </div>
                        <div class="icon-box">
                            <img src="<?=$bundle->baseUrl?>/images/office-work-with-time.png" alt="">
                        </div>
                        <div class="short-description">
                            Morbi in sem quis dui placerat ornare. Pellentesque
                            odio nisi, euismod in, pharetra a, ultricies in, diam.
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="facility-box" data-mh="1">
                        <div class="small-title" data-mh="2">
                            Vivamus <br />
                            vestibulum <br />
                        </div>
                        <div class="icon-box">
                            <img src="<?=$bundle->baseUrl?>/images/quality.png" alt="">
                        </div>
                        <div class="short-description">
                            Morbi in sem quis dui placerat ornare. Pellentesque
                            odio nisi, euismod in, pharetra a, ultricies in, diam.
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="facility-box" data-mh="1">
                        <div class="small-title" data-mh="2">
                            Vivamus <br />
                            vestibulum
                        </div>
                        <div class="icon-box">
                            <img src="<?=$bundle->baseUrl?>/images/male-telemarketer.png" alt="">
                        </div>
                        <div class="short-description">
                            Morbi in sem quis dui placerat ornare. Pellentesque
                            odio nisi, euismod in, pharetra a, ultricies in, diam.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="products">
    <div class="container">
        <div class="text-center">
            <div class="title">
                Produse noi
            </div>
        </div>
        <div class="products-group">
            <div class="row">
                <?php
                foreach ($newProducts as $product){
                    ?>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="product-box" data-mh="3">
                            <div class="img" data-mh="14">
                                <span class="badge">NOU</span>
                                <?= Html::img($product->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                            </div>
                            <div class="name-product" data-mh="4">
                                <?=$product->lang->Name?>
                            </div>
                            <div class="price">
                                <?=$product->Price?> Lei
                            </div>
                            <div class="text-center">
                                <a href="<?=\yii\helpers\Url::to(['/catalog/product','slug'=>$product->lang->Slug])?>" class="see-more">
                                    MAI MULTE DETALII
                                </a>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>


            </div>
        </div>
    </div>
</section>

<section class="presentation middle">
    <div class="container">
        <div class="presentation-elem">
            <div class="content">
                <div class="big-title">
                    Aliquam tincidunt <br />
                    mauris eu risus.
                </div>
                <div class="short-description">
                    Morbi in sem quis dui placerat ornare. Pellentesque odio nisi,
                    euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras
                    consequat. Praesent dapibus, neque id cursus faucibus, tortor
                    neque egestas augue, eu vulputate magna eros eu erat.
                    Aliquam erat volutpat. Nam dui mi, tincidunt quis,
                    accumsan porttitor, facilisis luctus, metus.
                </div>
                <div class="text-center">
                    <a href="#">
                        MAI MULTE DETALII
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="products recent-add-products">
    <div class="container">
        <div class="text-center">
            <div class="title">
                Produse cu reducere
            </div>
        </div>
        <div class="products-group">
            <div class="row">
                <?php
                foreach ($saleProducts as $product){
                    ?>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="product-box" data-mh="3">
                            <div class="img" data-mh="14">
                                <span class="badge">Sale</span>
                                <?= Html::img($product->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                            </div>
                            <div class="name-product" data-mh="4">
                                <?=$product->lang->Name?>
                            </div>
                            <div class="price">
                                <?=$product->Price?> Lei
                            </div>
                            <div class="text-center">
                                <a href="<?=\yii\helpers\Url::to(['/catalog/product','slug'=>$product->lang->Slug])?>" class="see-more">
                                    MAI MULTE DETALII
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>

<section class="about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="image-box-about-us" data-mh="5">
                    <img class="img-responsive" src="<?=$bundle->baseUrl?>/images/middle-presentation-bg.png" alt="">
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="description-company" data-mh="5">
                    <div class="small-title">
                        Despre noi.
                    </div>
                    <div class="description">
                        <div class="mt15">
                            <?=Settings::getByName('aboutUsShort',true)?>
                        </div>
                    </div>
                    <a href="/info/contacts" class="see-more contact-us">
                        CONTACTATI-NE
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog">
    <div class="container">
        <div class="text-center">
            <div class="title">
                Blog
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($news as $key=>$new){
                if ($key == 1){
                ?>
                <div class="col-md-4" data-mh="5">
                    <div class="blog-box" >
                        <a href="<?=\yii\helpers\Url::to(['/blog/post','id'=>$new->ID])?>">
                            <div class="img">
                                <?= Html::img($new->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                            </div>
                            <div class="info">
                                <div class="small-title">
                                   <?=$new->lang->Title?>
                                </div>
                                <div class="date-post">
                                    <?=$new->niceDate?>
                                </div>
                                <div class="description">
                                    <?=$new->shortContent?>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            <?php
            }else{
                    ?>
                    <div class="col-md-4" data-mh="5">
                        <div class="blog-box" >
                            <a href="<?=\yii\helpers\Url::to(['/blog/post','id'=>$new->ID])?>">
                                <div class="info">
                                    <div class="small-title">
                                        <?=$new->lang->Title?>
                                    </div>
                                    <div class="date-post">
                                        <?=$new->niceDate?>
                                    </div>
                                    <div class="description">
                                        <?=$new->shortContent?>
                                    </div>
                                </div>
                                <div class="img">
                                    <?= Html::img($new->mainImage->imagePath, ['width' => 550, 'class' => 'img-responsive']) ?>
                                </div>
                            </a>
                        </div>
                    </div>
            <?php
                }

            }
            ?>

        </div>
    </div>
</section>
<section class="subscribe">
    <div class="container">
        <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
            <div class="col-md-6" >
                <div class="title">
                    Aboneaza-te la newsletter
                </div>
            </div>
            <div class="col-md-6" style="padding-top: 35px;">

                <?php Pjax::begin([
                    'enablePushState' => false,
                    'enableReplaceState'=>false,

                ])?>
                <?php $form = ActiveForm::begin([
                    'action' => ['/ajax/add-newsletter'],
                    'method' => 'post',
                    'id'=> 'newsletter',
                    'fieldConfig' =>[
                        'options' => [
                            'tag' => false,
                        ]
                    ],
                    'options' => [

                        'class' => 'subscribe-form',
                        'data-pjax' => true,
                    ]
                ]); ?>

                <?= $form->field($newsletter, 'Email',['template' => "{input}"])->input('text',[
                    'placeholder' => 'Email',
                    'class' => 'nws-inpt',

                ])->label(false); ?>


                <?= Html::submitButton(Yii::t('app', 'Abonare'), ['class' => 'btn  newsletter-btn']) ?>


                <?php ActiveForm::end(); ?>
                <?php Pjax::end()?>
            </div>
        </div>
    </div>
</section>

<section class="parteners-and-payout">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="payout-group" data-mh="8">
                    <div class="small-title">
                        Metoda <br />
                        de plata
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/pay-pal.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/visa.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/master-card.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/western-union.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="payout-box">
                                <img src="<?=$bundle->baseUrl?>/images/maestro-card.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="parteners" data-mh="8">
                    <div class="small-title">
                        Partenerii <br />
                        nostri
                    </div>
                    <div class="swiper-container parteners-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/facebook-img.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/apple-music.png" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img src="<?=$bundle->baseUrl?>/images/windows-10.png" alt="">
                            </div>
                        </div>
                        <!-- Add BUTTON NAV -->
                        <div class="swiper-button-next parteners-next-btn">
                            <img src="<?=$bundle->baseUrl?>/images/slider-next.png" alt="">
                        </div>
                        <div class="swiper-button-prev parteners-prev-btn">
                            <img src="<?=$bundle->baseUrl?>/images/slider-prev.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="seo">
    <div class="container">
        <div class="title">
            SEO
        </div>
        <div class="description">
            <div class="row">
                <div class="col-md-12">
                    <div class="mt20">
                        <?=Settings::getByName('footerText',true)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

