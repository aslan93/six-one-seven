<?php
use app\views\themes\shop617\assets\FrontAsset;
use yii\helpers\Url;
use app\modules\Filter\models\Filter;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$bundle = FrontAsset::register($this);
?>

<section class="name-page">
    <div class="container">
        <div class="big-title">
            <?=$product->lang->Name?>
        </div>
    </div>
</section>

<section class="about-product">
    <div class="container" id="item">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=Url::to(['/home'])?>">Acasa</a></li>
            <li class="breadcrumb-item"><a href="<?=Url::to(['/catalog'])?>">CATALOG</a></li>
            <li class="breadcrumb-item"><a href="<?=Url::to(['/catalog/category','cid'=>$product->CategoryID])?>"><?=$product->category->lang->Title?></a></li>
            <li class="breadcrumb-item active"><?=$product->lang->Name?></li>
        </ol>
        <div class="products-info">
            <div class="row">
                <div class="col-md-6">
                    <div class="swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            <?php
                            foreach ($product->images as $image) {
                                ?>
                                <div class="swiper-slide" >
                                    <?=\yii\bootstrap\Html::img($image->imagePath)?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            <?php
                            foreach ($product->images as $image) {
                                ?>
                                <div class="swiper-slide" >
                                    <?=\yii\bootstrap\Html::img($image->imagePath)?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="description-product">
                        <div class="title">
                            <?=$product->lang->Name?>
                        </div>
                        <div class="price">
                            <?=$product->Price?> Lei
                        </div>
                    </div>
                    <?php Pjax::begin([
                        'id'=> 'cart-pjax',
                        'enablePushState' => false,
                        'enableReplaceState'=>false,
                    ])?>
                    <?php $form = ActiveForm::begin([
                        'action'=> Url::to(['/admin/ecommerce/cart/cart/add-to-cart','id' => $product->ID]),
                        'method' => 'get',
                        'id'=> 'add-to-cart',
                        'fieldConfig' =>[
                            'options' => [
                                'tag' => false,
                            ]
                        ],
                        'options' => [

                            'class' => 'form-add-to-cart',
                            'data-pjax' => true,
                        ]
                    ]); ?>
                        <?php
                        if ($productColors) {
                            ?>
                        <div class="row mt60 ">
                                <div class="col-md-3">
                                    <label>
                                        <?php
                                        echo $productColors[0]->filter->lang->Name;
                                        ?>
                                    </label>
                                </div>
                                <div class="col-md-9">
                                    <?php
                                    foreach ($productColors as $color) {
                                        ?>
                                        <input id="color<?= $color->ID ?>" value="<?= $color->ID ?>" name="Color" class="color-input"
                                               type="radio" required="required">
                                        <label class="color " for='color<?= $color->ID ?>'
                                               style='background-image:url(<?= $color->color->colorImage ?>)'></label>
                                        <?php
                                    }
                                    ?>
                                </div>
                        </div>
                        <?php
                        }
                        foreach ($productFilters as $filter){
                            if ($filter->Type != Filter::TypeColor){
                                $pfvs = Filter::getFilterProductValues($product->ID,$filter->ID);
                                        ?>

                                <div class="row mt60">
                                    <div class="col-md-3">
                                        <label>
                                            <?php
                                            echo $filter->lang->Name;
                                            ?>
                                        </label>
                                    </div>
                                    <div class="col-md-9">
                                        <?php

                                        foreach ($pfvs as $pfv) {
                                            ?>
                                            <input id="<?=$pfv->Value?><?=$pfv->ID?>" name="<?=$filter->lang->Name?>" value="<?=$pfv->ID?>" type="radio" required="required">
                                            <label class="attribut" for="<?=$pfv->Value?><?=$pfv->ID?>"><?=$pfv->Value?></label>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>
                    <div class="row mt60 ">
                        <div class="col-md-3">
                            <label>
                                Cantitate
                            </label>
                        </div>
                        <div class="col-md-3">
                            <div class="product"><div class="body-product">
                            <div class="input-group">
                                              <span class="input-group-btn">
                                                  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[<?=$product->ID?>]">
                                                      <span class="fa fa-caret-left"></span>
                                                  </button>
                                              </span>

                                <?= Html::input('number', 'quantity', 1, [
                                    'max'=>100,
                                    'min'=>1,
                                    'id' => 'quant['. $product->ID.']',
                                    'class' => 'form-control input-number',
                                ]) ?>
                                <span class="input-group-btn">
                                                  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[<?=$product->ID?>]">
                                                      <span class="fa fa-caret-right"></span>
                                                  </button>
                                              </span>
                            </div>
                                </div></div>
                        </div>
                    </div>

                    <div class="submit-form-details">
                        <?=Html::SubmitButton('Adauga in cos',['class'=>'btn btn-default'])?>
                    </div>

                <?php ActiveForm::end(); ?>
                <?php Pjax::end()?>

            </div>
            <div class="row">
                <div class="col-md-8">
                    <?php
                    if ($product->lang->Description) {
                        ?>
                        <div class="small-different-title">
                            Descriere
                        </div>
                        <div class="text">
                            <?= $product->lang->Description ?>
                        </div>
                        <?php
                    }
                    if ($caracteristics) {
                        ?>
                        <div class="small-different-title">
                            CARACTERISTICI ȘI BENEFICII
                        </div>
                        <ul>
                            <?php
                            foreach ($caracteristics as $caracteristic) {
                                echo '<li class="arrow">' . $caracteristic . '</li>';
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    if ($specifications) {
                        ?>
                        <div class="small-different-title">
                            SPECIFICATII
                        </div>
                        <ul>
                            <?php
                            foreach ($specifications as $specification) {
                                echo '<li class="arrow">' . $specification . '</li>';
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-4">
                    <div class="product-group">
                        <?php
                        if ($recomandations) {
                            ?>
                            <div class="small-different-title">
                                RECOMANDATE PENTRU TINE
                            </div>
                            <?php
                            foreach ($recomandations as $recomandation) {
                                ?>
                                <div class="product-box" data-mh="3">
                                    <div class="img">
                                        <?php
                                        if ($recomandation->MarkNew == 1 && $recomandation->MarkSell == 0) {
                                            ?>
                                            <span class="badge">NOU</span>
                                            <?php
                                        } elseif ($recomandation->MarkSell == 1) {
                                            ?>
                                            <span class="badge">Sale</span>
                                            <?php
                                        }
                                        ?>

                                        <?= \yii\bootstrap\Html::img($recomandation->mainImage->imagePath, ['class' => 'img-responsive']) ?>
                                    </div>
                                    <div class="name-product" data-mh="4">
                                        <?= $recomandation->lang->Name ?>
                                    </div>
                                    <div class="price">
                                        <?= $recomandation->Price ?> Lei
                                    </div>
                                    <div class="text-center">
                                        <a href="<?= \yii\helpers\Url::to(['/catalog/product', 'slug' => $recomandation->lang->Slug]) ?>"
                                           class="see-more">
                                            MAI MULTE DETALII
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->registerJs("
    $('.color-input').click(function(e){

        $.get('" . Url::to(['color-image']) . "', {cid: $(this).val()}, function(html){
                $('.swiper-slide-active img').attr('src',html);    
        });
    });
    
    $(document).on('change', '.s2attribute', function(){
        
        $('#pfvimage-container').empty();
        
        setTimeout(function(){
        
             $.post('" . Url::to(['color-image']) . "', {id: $('.s2attribute').attr('pfv-id'),val: $('.s2attribute').val(),fid: $('#filter').val(),productID:$('.productID').attr('data-attribute')}, function(html){
            
            $('#swiper-slide-image').html(html);
            
        });
    }, 500);
        
        
    });
    $('#cart-pjax').on('pjax:end',function(){

        $.pjax.reload({container:'#cart-info-pjax'});

    })

") ?>
