<?php
use app\views\themes\shop617\assets\FrontAsset;
use yii\helpers\Url;
use yii\widgets\ListView;

$bundle = FrontAsset::register($this);
?>
<section>
<section class="name-page">
    <div class="container">
        <div class="big-title">
            <?=$category->lang->Title?>
        </div>
    </div>
</section>

<div class="category">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?=Url::to(['/home'])?>">Acasa</a></li>
            <li class="breadcrumb-item"><a href="<?=Url::to(['/catalog'])?>">CATALOG</a></li>
            <li class="breadcrumb-item active"><?=$category->lang->Title?></li>
        </ol>
        <section class="products different">
            <div class="products-group">
                <div class="row">
                    <?=
                    ListView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{summary}\n{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                        'itemView' => '_product_item',
                    ]);
                    ?>
                </div>
            </div>
        </section>
    </div>
</div>
</section>