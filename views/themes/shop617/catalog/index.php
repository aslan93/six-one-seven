<?php

use app\views\themes\shop617\assets\FrontAsset;
use yii\widgets\ListView;
use app\modules\Product\models\Product;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\web\View;
$bundle = FrontAsset::register($this);

?>


<section class="name-page">
    <div class="container">
        <div class="big-title">
            Catalog
        </div>
    </div>
</section>

<div class="all-products">
    <div class="container">
        <div class="breadcurbs-filter-result">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Acasa</a></li>
                        <li class="breadcrumb-item active">CATALOG</li>
                    </ol>
                </div>
                <div class="col-md-4 col-sm-4" id="flag">
                    <div class="filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text">FILTREAZĂ DUPĂ: </div>
                            </div>
                            <div class="col-md-6">
                                <?php $form = ActiveForm::begin([
                                    'action'=> '/catalog#flag',
                                    'method' => 'get',
                                    'id'=>'search-forma',

                                ]); ?>
                                <?= $form->field($searchModel, 'sortParam')->widget(Select2::className(),[
                                    'data' => Product::getSortParams('Implicit'),
                                    'class' => 'style-select',
                                    'hideSearch'=> true,
                                    'options' => ['onchange' => 'submitForm("#search-form,#search-forma")']

                                ])->label(false)?>
                                <?php
                               ActiveForm::end();
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5">
                    <div class="result-search">
<!--                        SUNT AFISATE <span class="strong">9</span> DIN <span class="strong">40</span> PRODSE-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-5">
                <div class="left-slider-filter">
                    <a href="#" class="toggle-sidebar">
                        <span class="fa fa-navicon"></span>
                    </a>
                    <?php
                    echo $this->render('_search', ['model' => $searchModel]);
                    ?>
                </div>
            </div>
            <div class="col-md-9 col-sm-7">
                <div class="products-group no-padding">
                    <div class="row">
                        <?php
                        Pjax::begin([
                            'id' => 'catalog',

                        ])
                        ?>
                        <?=
                        ListView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{summary}\n{items}\n<span class='clearfix'></span><div class='pull-right'>{pager}</div>\n",
                            'itemView' => '_product_item',

                        ]);
                        ?>

                        <?php
                        Pjax::end();
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<?php $this->registerJs(
    "   function submitForm(id){
                var form = $(id);
                var formData = form.serialize();
                $.pjax.reload({container: '#catalog',data: formData, push:false,replace:false,timeout:10000}).success(function(){
                    $('#catalog').matchHeight();
                });
            }
         $(document).on('pjax:end','#catalog',function(){
            $('.product-box').matchHeight();
         });


            "
    , View::POS_END);
?>