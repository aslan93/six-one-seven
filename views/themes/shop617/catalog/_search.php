<?php
use yii\bootstrap\ActiveForm;
use app\modules\Category\models\Category;
use kartik\select2\Select2;
use app\modules\Attribute\models\Attribute;
use app\modules\Filter\models\Filter;
use app\modules\Filter\models\ProductFilterValue;
?>
<?php $form = ActiveForm::begin([
    'action'=> '#',
    'method' => 'post',
    'id'=>'search-form',

]); ?>

    <div class="small-title mt0">
        Categorie
    </div>
        <div>
        <?= $form->field($model, "CategoryID")->checkboxList(Category::getList())->label(false)?>
        </div>
        <?php
        foreach (Filter::find()->with('lang','productFilterValues')->all() as $filter){
           ?>
            <div class="small-title">
                <?=$filter->lang->Name?>
            </div>
            <?php
                if($filter->Type == Filter::TypeSelect){
            ?>
                <div>
                    <?= $form->field($model, "Filter[$filter->ID]")->widget(Select2::className(),[
                        'data' => ProductFilterValue::getList('Alege',$filter->ID),
                        'class' => 'style-select',
                    ])->label(false)?>
                </div>
            <?php
            }else{
            ?>
                <div>
                    <?= $form->field($model, "Filter[$filter->ID]")->checkboxList(ProductFilterValue::getList(false,$filter->ID))->label(false)?>
                </div>
            <?php
            }
        }
        ?>

    <?php ActiveForm::end(); ?>
<button class="btn btn-default" type="submit" onclick="submitForm('#search-form,#search-forma')" >
    APLICA
</button>