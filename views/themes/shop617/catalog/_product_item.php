<?php
use app\views\themes\shop617\assets\FrontAsset;

$bundle = FrontAsset::register($this);
?>

<div class="col-md-4">
    <div class="product-box" data-mh="3">
        <div class="img">
            <?php
            if ($model->MarkNew == 1 && $model->MarkSell == 0){
                ?>
                <span class="badge">NOU</span>
            <?php
            }elseif($model->MarkSell == 1){
                ?>
                <span class="badge">Sale</span>
            <?php
            }
            ?>

            <?=\yii\bootstrap\Html::img($model->mainImage->imagePath,['class'=>'img-responsive'])?>
        </div>
        <div class="name-product" data-mh="4">
            <?=$model->lang->Name?>
        </div>
        <div class="price">
            <?=$model->Price?> Lei
        </div>
        <div class="text-center">
            <a href="<?=\yii\helpers\Url::to(['/catalog/product','slug'=>$model->lang->Slug])?>" class="see-more">
                MAI MULTE DETALII
            </a>
        </div>
    </div>
</div>
