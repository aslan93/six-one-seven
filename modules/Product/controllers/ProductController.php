<?php

namespace app\modules\Product\controllers;

use app\modules\Filter\models\Filter;
use app\modules\Filter\models\ProductFilterValue;
use app\modules\Filter\models\PVFImage;
use app\modules\Product\models\ProductLang;
use Yii;
use app\modules\Product\models\Product;
use app\modules\Product\models\ProductSearch;
use app\controllers\SiteController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use app\modules\Product\models\ProductImage;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use dosamigos\transliterator\TransliteratorHelper;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $dataProvider = new ActiveDataProvider([
            'query' => ProductFilterValue::find()->where(['ProductID' => $model->ID]),
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['update',
                'id' => $model->ID,
                'dataProvider' => $dataProvider,
                ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => ProductFilterValue::find()->where(['ProductID' => $model->ID,'LangID'=>Yii::$app->language])->with('pFVImage','filter.lang'),
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveImage($model);
            return $this->redirect(['update',
                'id' => $model->ID,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'modelPFV' => (empty($modelPFV)) ? [new ProductFilterValue()] : $modelPFV,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveImage($model){

        $images = UploadedFile::getInstancesByName('Images');

        if($images){

            foreach ($images as $image)
            {
                $file = md5(microtime(true)) . '.' . $image->extension;
                $thumb = 'thumb_' . $file;

                if ($image->saveAs(Yii::getAlias("@webroot/uploads/product/$file")))
                {   Image::thumbnail(Yii::getAlias("@webroot/uploads/product/$file"), 200, 200)->save(Yii::getAlias("@webroot/uploads/product/$thumb"));

                    if ($model->mainImage) {
                        $imageModel = new ProductImage([
                            'ProductID' => $model->ID,
                            'Thumb' => $thumb,
                            'Image' => $file,
                            'IsMain' => 0,
                        ]);
                    }else{
                        $imageModel = new ProductImage([
                            'ProductID' => $model->ID,
                            'Thumb' => $thumb,
                            'Image' => $file,
                            'IsMain' => 1,
                        ]);
                    }
                    $imageModel->save();
                }
            }
        }
    }
    public function savePFVImage($model){

        $colorImage = UploadedFile::getInstanceByName('ColorImage');
        $productImage = UploadedFile::getInstanceByName('ProductImage');

        if($colorImage){

                $file1 = md5(microtime(true)) . '.' . $colorImage->extension;
                $file2 = md5(microtime(true)) . '-prod.' . $productImage->extension;

                if ($colorImage->saveAs(Yii::getAlias("@webroot/uploads/productColors/$file1" )))
                {   $productImage->saveAs(Yii::getAlias("@webroot/uploads/productColors/$file2"));
                        $imageModel = new PVFImage([
                            'PFVID' => $model->UniqID,
                            'ColorImage' => $file1,
                            'ProductImage' => $file2,
                        ]);

                    $imageModel->save();
                }

        }elseif($productImage){
            $pfvModel = ProductFilterValue::find()->where(['Value'=>$model->Value])->one();
            $oldImageModel = $pfvModel->pFVImage;
            $file2 = md5(microtime(true)) . '-prod.' . $productImage->extension;
            $productImage->saveAs(Yii::getAlias("@webroot/uploads/productColors/$file2"));
            $imageModel = new PVFImage([
                'PFVID' => $model->UniqID,
                'ColorImage' => $oldImageModel->ColorImage,
                'ProductImage' => $file2,
            ]);
            $imageModel->save();
        }
    }
    public function actionImageDelete(){
        $key = Yii::$app->request->post('key');
        ProductImage::findOne($key)->delete();
        return '{}';
    }
    public static function actionSetMainImage(){
        $key = Yii::$app->request->post('id');
        $imgModel = ProductImage::findOne($key);

        ProductImage::updateAll(['IsMain'=> 0],['ProductID'=>$imgModel->ProductID]);

        $imgModel->IsMain = 1;
        $rs = $imgModel->save();

        return "$rs";
    }
    public function actionGetItem()
    {
        $id = (int)Yii::$app->request->post('id');
        $productID = (int)Yii::$app->request->post('productID');
        $model = Product::findOne($productID);
        $pfv = $id > 0 ? ProductFilterValue::findOne($id) : new ProductFilterValue();

        return $this->renderAjax('product-filter', [
            'pfv' => $pfv,
            'productID' => $productID,
            'model'=> $model,
        ]);
    }

    public function actionSaveItem()
    {

        if (isset(Yii::$app->request->post('ProductFilterValue')['ProductID'])){
            $pid = Yii::$app->request->post('ProductFilterValue')['ProductID'];
            $fid = Yii::$app->request->post('ProductFilterValue')['FilterID'];
            $values = Yii::$app->request->post('ProductFilterValue')['Value'];
            $uniqID = uniqid('',true);
            $uid = Yii::$app->request->post('ProductFilterValue')['ID'];
            if (isset($uid)){
                ProductFilterValue::deleteAll(['UniqID'=>$uid]);
            }
            foreach($values as $key => $value){
                $model = new ProductFilterValue([
                    'ProductID' => $pid,
                    'FilterID' => $fid,
                    'LangID' => $key,
                    'Value' => $value,
                    'UniqID' => $uniqID,

                ]);
                $model->save();
                $this->savePFVImage($model);
            }
        }

    }

    public function actionDeleteChildItem()
    {   $id = Yii::$app->request->post('id');
        $model = ProductFilterValue::findOne($id);
        ProductFilterValue::deleteAll(['UniqID'=>$model->UniqID]);
        return '{1}';
    }

    public function actionGetPfv($lang,$pid = false)
    {
           $selected = '';
           if ($pid){
               $pfv = ProductFilterValue::find()->where(['ID'=>$pid])->one();
               $tpfv = ProductFilterValue::find()->where(['UniqID'=>$pfv->UniqID,'LangID'=>$lang])->one();
               $selected = $tpfv->Value;
           }
        if (isset($_POST['depdrop_parents'])) {
            $id = $_POST['depdrop_parents'];

            if(!$id[0]){ $id[0] = -1;}
            $out = ProductFilterValue::find()->where(['FilterID'=>$id[0],'LangID'=>$lang])->groupBy('Value')->all();
            $result =[];

            foreach ($out as  $model){
                $result[] = ['id'=> $model->Value,'name'=> $model->Value];

            }
            echo Json::encode(['output'=>$result, 'selected'=>$selected]);


        }else{
            echo '{}';
        }

    }

    public function actionGetImageInput(){
        $copied = 0;
        $fid = (int)Yii::$app->request->post('fid');
        $val = Yii::$app->request->post('val');
        $filter = Filter::findOne($fid);
        if ($filter->Type == Filter::TypeColor){
            $id = (int)Yii::$app->request->post('id');
            $productID = (int)Yii::$app->request->post('productID');
            $pfv = $id > 0 ? ProductFilterValue::findOne($id) : new ProductFilterValue();
                if ($pfv->Value == $val){

                }else{
                $pfv = ProductFilterValue::find()->where(['Value'=>$val,'ProductID' =>$productID])->one();
                    if (!isset($pfv->colorImage)) {
                        $pfv = ProductFilterValue::find()->where(['Value' => $val])->with('pFVImage')->one();
                        $copied = true;
                    }
                }

            return $this->renderAjax('color-img-form', [
                'productID' => $productID,
                'model'=> $pfv,
                'copied' => $copied,
            ]);
        }else{
            return '';
        }
    }


}
