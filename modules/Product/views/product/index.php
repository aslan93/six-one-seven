<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use app\modules\Attribute\models\Attribute;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=> 'category.lang.Title',
                'label' => 'Categorie',
            ],
            [
                'attribute'=> 'lang.Name',
                'label' => 'Produs',
            ],


            [
                'attribute'=> 'Price',
                'label' => 'Pret',
            ],

            ['class' => 'app\components\GridView\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
