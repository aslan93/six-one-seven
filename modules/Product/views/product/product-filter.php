<?php

use yii\bootstrap\Html;
use kartik\select2\Select2;
use yii\helpers\Url;
use app\modules\Product\models\Product;
use yii\widgets\ActiveForm;
use app\modules\Filter\models\Filter;
use app\modules\Filter\models\ProductFilterValue;
use kartik\depdrop\DepDrop;
use yii\web\JsExpression;
use kartik\file\FileInput;

?>
<div>
<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'id' => 'save-child-item-form',
    'action' => Url::to(['save-item'])
]);

?>

<div class="row">
    <?php
    if ($pfv->isNewRecord){
    }else{
        ?>
        <?= $form->field($pfv, 'UniqID')->hiddenInput(['value'=>$pfv->UniqID,'id'=>'uniqID'])->label(false) ?>
    <?php
    }
    ?>
    <?= $form->field($pfv, 'ProductID')->hiddenInput(['value'=>$productID])->label(false) ?>
    <div class="col-md-6">
        <?= $form->field($pfv, 'FilterID')->dropDownList(Filter::getList('Selecteaza'), [
            //'data' => Filter::getList('Selecteaza'),
            //'options' => [
                'id' => 'filter',
                'multiple' => false,
                'value' => $pfv->FilterID,
            //]
        ])->label('Filtrul:'); ?>


    </div>
    <div class="col-md-6">
        <?php
        foreach (Yii::$app->params['lang'] as $key => $lang){

            ?>
            <div class="col-sm-12">
                <?php

                echo $form->field($pfv, "Value[$key]")->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $pfv->getFilterProductValue($key),
                    'options' => ['id' => $key.'value','class' => 's2attribute','pfv-id'=>$pfv->ID],

                    'pluginOptions' => [
                        'depends' => ['filter'],
                        'placeholder' => 'Select...',
                        'url' => Url::to(['/admin/ecommerce/product/product/get-pfv','lang' =>$key,'pid'=>$pfv->ID])
                    ],

                    'select2Options' => [
                        'pluginOptions' => [
                            'multiple' => false,
                            'tags' => true,
                            'allowClear' => true
                        ]
                    ],
                ])->label('Atribute ('.$key.'):'); ?>
            </div>
            <?php
        }
        ?>

    </div>
    <div class="col-md-12" id="pfvimage-container">

    </div>
    <div class="hidden">
        <?=Select2::widget([
            'name' => 'hidden',
            'data' => ['1','2'],
            'pluginOptions' => [
                'initSelection' => new JsExpression("function(event) {
     
                           //$('#filter').trigger('change');
                          // $('.s2attribute').trigger('change');
                           //$('.s2attribute').select2('data');
                            return 1;
                         }"),
            ]
        ])?>
    </div>
</div>
    <br>
    <br>
    <div class="form-group">
        <?= Html::button($pfv->isNewRecord ? Yii::t("app", 'Adauga') : Yii::t("app", 'Update'), ['class' =>  'btn btn-primary', 'id' => 'save-child-item-button']) ?>
    </div>
<?php ActiveForm::end(); ?>

<?php $this->registerJs("
    $('#save-child-item-button').click(function(e){
        $.ajax({
            url: '" . Url::to(['save-item']) . "',
            data: new FormData($('#save-child-item-form')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            success: function(){
            $('#edit-slider-item-modal').modal('hide');
            $.pjax.reload({container:'#filters-pjax', 
            timeout: false,
            complete: function() {
                
            },
            
        });

            }
        });
    });
    
    $(document).on('change', '.s2attribute', function(){
        //alert('attr');
        $('#pfvimage-container').empty();
        
        setTimeout(function(){
        
             $.post('" . Url::to(['get-image-input']) . "', {id: $('.s2attribute').attr('pfv-id'),val: $('.s2attribute').val(),fid: $('#filter').val(),productID:$('.productID').attr('data-attribute')}, function(html){
            
            $('#pfvimage-container').html(html);
            
        });
    }, 500);
        
        
    });

") ?>
</div>