<?php
use app\modules\Location\models\Location;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;

?>


<?= $form->field($langModel, "[$langModel->LangID]Name")->textInput() ?>
<?= $form->field($langModel, "[$langModel->LangID]Description")->textArea()->widget(TinyMce::className(),[
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
       'theme' => 'modern',
    ]
]) ?>
<?= $form->field($langModel, "[$langModel->LangID]Caracteristics")->textArea()->widget(TinyMce::className(),[
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'theme' => 'modern',
    ]
]) ?>
<?= $form->field($langModel, "[$langModel->LangID]Specifications")->textArea()->widget(TinyMce::className(),[
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'theme' => 'modern',
    ]
]) ?>
