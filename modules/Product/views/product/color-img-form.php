<?php
use yii\helpers\Html;
use kartik\file\FileInput;

$initialPreview1 = [];
$initialPreviewConfig1 = [];
$initialPreview2 = [];
$initialPreviewConfig2 = [];

if ($model->pFVImage) {
    $initialPreview1[] = Html::img($model->pFVImage->colorImage, ['width' => 200]);

//    $initialPreviewConfig1[] = [
//        'url' => \yii\helpers\Url::to(['/ecommerce/product/product/image-delete']),
//        'key' => $model->pFVImage->ID,
//    ];
    if ($copied == 0) {
        $initialPreview2[] = Html::img($model->pFVImage->productImage, ['width' => 200]);
//        $initialPreviewConfig2[] = [
//            'url' => \yii\helpers\Url::to(['/ecommerce/product/product/image-delete']),
//            'key' => $model->pFVImage->ID,
//        ];
    }
}

?>
<br>
<div class="row">
    <div class="col-md-6">
        <?php
        echo FileInput::widget([
            'name' => 'ColorImage',
            'id' => 'color-image',
            'attribute' => 'ColorImage',
            'options'=>['accept'=>'image/*','multiple' => false],
            'pluginOptions' => [
                'overwriteInitial'=>false,
                'maxFileSize'=>2800,
                'fileActionSettings' => [
                    'fileActionSettings' => [
                        'showZoom' => false,
                        'showDelete' => true,
                    ],
                ],
                'browseClass' => 'btn btn-success',
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview1,
                'initialPreviewConfig' => $initialPreviewConfig1,
            ],
        ]);
        ?>

    </div>
    <div class="col-md-6">
        <?php
        echo FileInput::widget([
            'name' => 'ProductImage',
            'id' => 'product-image',
            'attribute' => 'ProductImage',
            'options'=>['accept'=>'image/*','multiple' => false],
            'pluginOptions' => [
                'overwriteInitial'=>false,
                'maxFileSize'=>2800,
                'fileActionSettings' => [
                    'fileActionSettings' => [
                        'showZoom' => false,
                        'showDelete' => true,
                    ],
                ],
                'browseClass' => 'btn btn-success',
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview2,
                'initialPreviewConfig' => $initialPreviewConfig2,
            ],
        ]);
        ?>

    </div>
</div>