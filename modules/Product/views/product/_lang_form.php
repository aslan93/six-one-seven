<?php
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\modules\Attribute\models\Attribute;



use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\web\View;
use kartik\depdrop\DepDrop;



$url =  Url::to('@web/uploads/doors-colors/');
$format = <<< SCRIPT
function format(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url' +  state.text
    return '<img class="image" src="' + src + '"/>';
    
}
SCRIPT;
$url4 =  Url::to('@web/uploads/doors-handles/');
$format4 = <<< SCRIPT
function format4(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url4' +  state.text
    return '<img class="image" src="' + src + '"/>';
    
}
SCRIPT;
$url2 =  Url::to('@web/uploads/decoration/');
$format2 = <<< SCRIPT
function format2(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url2' +  state.text
    return '<img class="image" src="' + src + '"/>';
}
SCRIPT;

$url3 =  Url::to('@web/uploads/material/');
$format3 = <<< SCRIPT
function format3(state) {
    if (!state.id) return state.text; // optgroup
    src = '$url3' +  state.text
    return '<img class="image" src="' + src + '"/>';
}
SCRIPT;

$escape = new JsExpression("function(m) { return m; }");

$this->registerJs($format, View::POS_HEAD);
$this->registerJs($format2, View::POS_HEAD);
$this->registerJs($format3, View::POS_HEAD);
$this->registerJs($format4, View::POS_HEAD);



?>
<style>
    .image{
        width:50%;
        height:50%;
        position: relative;

    }
    .select2-container--krajee .select2-selection--single{
        height: auto;
    }
    .select2-container--krajee .select2-selection--single .select2-selection__arrow{
        border-left: none!important;
    }
</style>
    <div class="row"><br>
        <div class="col-sm-6">
            <?php
            $items = [];
            foreach ($model->langs as $langID => $langModel)
            {
                $items[] = [
                    'label' => strtoupper($langID),
                    'content' => $this->render('_desc_form',[
                        'form' => $form,
                        'langModel' => $langModel,
                        'model' => $model,
                    ]),
                ];
            }
            echo '<br>';
            echo Tabs::widget([
                'items' => $items,
            ]);
            ?>
        </div>
        <div class="col-sm-6">
            <div class="col-md-6">
                <?= $form->field($model, 'Price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, "CategoryID")->dropDownList( \app\modules\Category\models\Category::getList() ,['id'=>'category'])->label('Categorie') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'MarkNew')->checkbox()->label('Marcheaza ca produs nou') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'MarkSell')->checkbox()->label('Marcheaza ca produs la reducere') ?>
            </div>
        </div>
    </div>


