<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\bootstrap\Html;

?>
<br>
<?php
if($model->isNewRecord){

}else {
    ?>
    <div>
        <?= Html::button('<i class="fa fa-plus"></i> ' . Yii::t("app", 'Add filter'), [
            'class' => 'btn btn-success edit-child-item',
            'slider-item-id' => 0
        ]) ?>
    </div>
    <br>
    <span class="hidden productID" data-attribute="<?=$model->ID?>"></span>
    <div>
        <?php
        \yii\widgets\Pjax::begin([
            'enablePushState'    => false,
            'enableReplaceState' => false,
            'id'                 => 'filters-pjax',
            'timeout'            => 5000,
        ])
        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'options' => [
                        'width' => '50px'
                    ]
                ],
                [
                    'label' => 'Filter',
                    'value' => function ($model) {
                        return $model->filter->lang->Name;
                    }
                ],
                [
                    'label' => 'Value',
                    'value' => function ($model) {
                        return $model->Value;
                    }
                ],
                [
                    'label' => 'Color Image',
                    'format' => 'html',
                    'value' => function ($model) {
                            if (isset($model->pFVImage)) {
                                return Html::img($model->pFVImage->colorImage, ['width' => 50, 'class' => 'color-image']);
                                }else{
                                return '';
                            }
                            }
                ],
                [
                    'label' => 'Product Image',
                    'format' => 'html',
                    'value' => function ($model) {
                        if (isset($model->pFVImage)) {
                            return Html::img($model->pFVImage->productImage, ['width' => 50, 'class' => 'color-image']);
                        }else{
                            return '';
                        }
                    }
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => [
                        'width' => '140px'
                    ],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::button('<i class="fa fa-pencil"></i>', [
                                'class' => 'btn btn-success btn-xs edit-child-item',
                                'child-item-id' => $model->ID,

                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fa fa-trash"></i>', '#', [
                                'class' => 'btn btn-danger btn-xs ',
                                'id' => 'delete-item',
                                'child-item-id' => $model->ID,

                            ]);
                        }
                    ],
                    'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>'
                ],
            ],
        ]); ?>
        <?php
        \yii\widgets\Pjax::end()
        ?>
    </div>

    <?php Modal::begin([
        'id' => 'edit-slider-item-modal',
        'header' => Yii::t("app", 'Editing child item'),
        'size' => Modal::SIZE_LARGE,
        'options'=>[
                'tabindex' => '',
        ]
    ]); ?>

    <?php Modal::end(); ?>

    <?php $this->registerJs("
    $(document).on('click', '.edit-child-item', function(){
        $('#edit-slider-item-modal .modal-body').empty();
        $.post('" . Url::to(['get-item']) . "', {id: $(this).attr('child-item-id'),productID:$('.productID').attr('data-attribute')}, function(html){
            
            $('#edit-slider-item-modal .modal-body').html(html);
            $('#edit-slider-item-modal').modal();
        }).done(function(){
            $('#filter').trigger('change');
            $('.s2attribute').trigger('change');
        });
    });
    $(document).on('click', '#delete-item', function(){
       
        $.post('" . Url::to(['delete-child-item']) . "', {id: $(this).attr('child-item-id')}, function(data){
          $.pjax.reload({container:'#filters-pjax', 
            timeout: false,
            complete: function() {
                
            },
            
        });
        });
    });

") ?>

    <?php
}
?>
