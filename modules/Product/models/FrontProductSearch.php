<?php

namespace app\modules\Product\models;

use app\components\DataProvider\CustomActiveDataProvider;
use app\modules\Attribute\models\Attribute;
use Yii;
use yii\base\Model;

use app\modules\Product\models\Product;
use yii\db\Expression;

/**
 * ProductSearch represents the model behind the search form about `app\modules\Product\models\Product`.
 */
class FrontProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public $sortParam = NULL;

    public $CategoryID = NULL;
    public $Filter = [];
    public function rules()
    {
        return [
            [['ID', 'Variable'], 'integer'],
            [['Price'], 'number'],
            [['sortParam', 'CategoryID','Filter'],'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return CustomActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->request->get()){
        $get = Yii::$app->request->get('FrontProductSearch');


        $query = Product::find()->joinWith('productFilterValues')->with(['lang','category','mainImage'])->groupBy('Product.ID');
        $expression = new Expression("count(ProductFilterValue.ID) as `count`");
        $query->addSelect(["Product.ID","Product.Price","Product.CategoryID", $expression]);


        $dataProvider = new CustomActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);




        $filters = array_filter($this->Filter);
        $count = count($filters);
        $query->having(['>=','count', $count]);
        foreach ($filters as $filter){
            if (is_array($filter)){
                $query->orFilterWhere(['ProductFilterValue.Value'=>$filter,'ProductFilterValue.LangID'=>Yii::$app->language]);
            }else {
                $query->orFilterWhere(['ProductFilterValue.Value' => (array)$filter,'ProductFilterValue.LangID'=>Yii::$app->language]);
            }
        }
        if($this->CategoryID) {
            $query->andFilterWhere(['CategoryID'=>(array)$this->CategoryID]);
        }


        if($this->sortParam == 3) {
            $query->orderBy(['Price' => 3]);
        }
        if($this->sortParam == 4) {
            $query->orderBy(['Price' => 4]);
        }

        return $dataProvider;
    }else {
        $attributesIDs = [];
        $query = Product::find()->joinWith('productFilterValues')->with(['lang', 'category', 'mainImage'])->groupBy('Product.ID');
        $expression = new Expression("count(ProductFilterValue.ID) as `count`");
        $query->addSelect(["Product.ID", "Product.Price", "Product.CategoryID", $expression]);


        $dataProvider = new CustomActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'ID' => $this->ID,
//            'CategoryID' => (array)$this->CategoryID,
//            'Price' => $this->Price,
//            'Variable' => $this->Variable,
//        ]);


        $filters = array_filter($this->Filter);
        $count = count($filters);
        $query->having(['>=', 'count', $count]);
        foreach ($filters as $filter) {
            if (is_array($filter)) {
                $query->orFilterWhere(['ProductFilterValue.Value' => $filter, 'ProductFilterValue.LangID' => Yii::$app->language]);
            } else {
                $query->orFilterWhere(['ProductFilterValue.Value' => (array)$filter, 'ProductFilterValue.LangID' => Yii::$app->language]);
            }
        }
        if ($this->CategoryID) {
            $query->andFilterWhere(['CategoryID' => (array)$this->CategoryID]);
        }

        //$query->andFilterWhere(['or', ['ProductAttribute.AttributeID'=>(array)$this->ColorID], ['ProductAttribute.AttributeID'=>$this->SizeID],['ProductAttribute.AttributeID'=>(array)$this->MaterialID]])->groupBy('Product.ID,ProductAttribute.AttributeID,AttrType');

//        if($this->ColorID) {
//            $query->andFilterWhere(['ProductAttribute.AttributeID'=>(array)$this->ColorID]);
//        }
//        if($this->SizeID) {
//            $query->andFilterWhere(['ProductAttribute.AttributeID'=>$this->SizeID]);
//        }
//        if($this->MaterialID) {
//            $query->andFilterWhere(['ProductAttribute.AttributeID'=>(array)$this->MaterialID]);
//        }
        if ($this->sortParam == 3) {
            $query->orderBy(['Price' => 3]);
        }
        if ($this->sortParam == 4) {
            $query->orderBy(['Price' => 4]);
        }

        return $dataProvider;
        }
    }
}
