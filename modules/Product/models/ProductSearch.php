<?php

namespace app\modules\Product\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Product\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\modules\Product\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'CategoryID'], 'integer'],
            [['Price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->with('lang','mainImage');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CategoryID' => $this->CategoryID,
            'Price' => $this->Price,
            'Variable' => $this->Variable,
        ]);

        return $dataProvider;
    }
}
