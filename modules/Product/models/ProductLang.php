<?php

namespace app\modules\Product\models;

use Yii;

/**
 * This is the model class for table "ProductLang".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property string $LangID
 * @property string $Name
 * @property string $Description
 */
class ProductLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'LangID', 'Name','Slug', 'Description'], 'required'],
            [['ProductID'], 'integer'],
            [['Description','Specifications','Caracteristics'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'LangID' => 'Lang ID',
            'Name' => 'Name',
            'Description' => 'Description',
            'Specifications' => 'Specifications',
            'Caracteristics' => 'Caracteristics',
            'Slug' => 'Slug',
        ];
    }
}
