<?php

namespace app\modules\Product\models;

use app\modules\Attribute\models\Attribute;
use app\modules\Cart\models\OrderProduct;
use app\modules\Filter\models\Filter;
use app\modules\Filter\models\ProductFilterValue;
use Yii;
use app\modules\Category\models\Category;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use app\modules\Attribute\models\ProductAttribute;
use dosamigos\transliterator\TransliteratorHelper;
use yii\caching\TagDependency;
/**
 * This is the model class for table "Product".
 *
 * @property integer $ID
 * @property integer $CategoryID
 * @property double $Price
 * @property integer $Variable
 *
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord
{
    public $ColorIDs = [];
    public $MaterialIDs = [];
    public $SizeIDs = [];
    const Sort_Desc_Price = 3;
    const Sort_Asc_Price = 4;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CategoryID'], 'integer'],
            [['Price'], 'number'],
            [['Variable','ColorIDs','MaterialIDs','SizeIDs','MarkNew','MarkSell','CategoryID'],'safe'],
            [['CategoryID'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['CategoryID' => 'ID']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CategoryID' => 'Category ID',
            'Price' => 'Price',
            'Variable' => 'Variable',
            'MarkNew' => 'New Product',
            'MarkSell' => 'Sale Product',
        ];
    }




    public function getLangs()
    {
        $langs = $this->hasMany(ProductLang::className(), ['ProductID' => 'ID'])->indexBy('LangID')->all();
        $result = [];
        foreach (array_keys(Yii::$app->params['lang']) as $LangID){
            $result[$LangID]= isset($langs[$LangID]) ? $langs[$LangID] : new ProductLang([
                'LangID' => $LangID,
            ]);
        }
        return $result;
    }

    public function getLang()
    {
        return $this->hasOne(ProductLang::className(), ['ProductID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    public function getImages(){

        return  $this->hasMany(ProductImage::className(), ['ProductID' => 'ID']);
    }
    public function getMainImage(){
        return  $this->hasOne(ProductImage::className(), ['ProductID' => 'ID'])->where(['IsMain'=>1]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        TagDependency::invalidate(Yii::$app->cache,  self::className());
        $langModels = $this->langs;
        foreach ($langModels as $langModel){

            $langModel->ProductID = $this->ID;
        }

        if(Model::loadMultiple($langModels,Yii::$app->request->post())){

            foreach ($langModels as $langModel){
                $slug = TransliteratorHelper::process($langModel->Name, '');
                $slug = str_replace(' ', '-', trim(strtolower($slug)));
                $langModel->Slug = $slug;
                $langModel->save();
            }
        }
    }

    public function getProductColors()
    {
        return $this->hasMany(ProductFilterValue::className(), ['ProductID' => 'ID'])
            ->joinWith('filter')
            ->where(['LangID'=>Yii::$app->language,'Filter.Type'=> Filter::TypeColor])->all();
    }
    public function getProductAttributes()
    {
        return $this->hasMany(ProductFilterValue::className(), ['ProductID' => 'ID'])->all();
    }

    public function getFilters()
    {
        $values = $this->hasMany(ProductFilterValue::className(), ['ProductID' => 'ID'])
            ->joinWith('filter')
            ->where(['LangID'=>Yii::$app->language])
            ->groupBy('Filter.ID')->all();

        $filters = [];
        foreach ($values as $key => $pfv){
            $filters[$key] = $pfv->filter;
        }
        return $filters;
    }

    public function getProductFilterValues(){
        return $this->hasMany(ProductFilterValue::className(), ['ProductID' => 'ID'])->with('filter.lang');
    }

    public static function getSortParams($addEmpty = null){
        $return = $addEmpty ? ['' => $addEmpty] : [];

        $list = [
            self::Sort_Desc_Price => Yii::t('app', 'Descendent dupa Pret'),
            self::Sort_Asc_Price => Yii::t('app', 'Ascendent dupa Pret'),
        ];
        return $return + $list;
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['ID' => 'CategoryID'])->with('lang');
    }

    public function getProductOrder(){
        return $this->hasMany(OrderProduct::className(), ['ProductID' => 'ID']);
    }
}
