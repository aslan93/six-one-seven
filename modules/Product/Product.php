<?php

namespace app\modules\Product;

use app\components\Module\AdminModule;

/**
 * Product module definition class
 */
class Product extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Product\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
