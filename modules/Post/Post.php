<?php

namespace app\modules\Post;

use app\components\Module\AdminModule;



/**
 * post module definition class
 */
class Post extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Post\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
