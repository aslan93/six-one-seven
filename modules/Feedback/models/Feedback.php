<?php

namespace app\modules\Feedback\models;

use Yii;

/**
 * This is the model class for table "Feedback".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Subject
 * @property string $Email
 * @property string $Message
 * @property string $Date
 */
class Feedback extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Subject', 'Email', 'Message'], 'required'],
            [['Message'], 'string'],
            [['Date'], 'safe'],
            [['Name', 'Subject', 'Email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t("app", 'ID'),
            'Name' => Yii::t("app", 'Name'),
            'Subject' => Yii::t("app", 'Subject'),
            'Email' => Yii::t("app", 'Email'),
            'Message' => Yii::t("app", 'Message'),
            'Date' => Yii::t("app", 'Date'),
        ];
    }

    public function getdMYDate()
    {
        return date('d M Y', strtotime($this->Date));
    }
    public function getShortText($length = 200)
    {
        return mb_substr(strip_tags(html_entity_decode($this->Message)), 0, $length);
    }
}
