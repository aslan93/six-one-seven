<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = $model->Subject;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbacks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;' . Yii::t('app', 'Back to list'), ['index'], [
            'class' => 'btn btn-info',
        ]) ?>
        &nbsp;&nbsp;
        <?= Html::a('<i class="fa fa-trash"></i> &nbsp;' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this feedback?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Name',
            'Subject',
            'Email:email',
            'Message:ntext',
            'Date',
        ],
    ]) ?>

</div>
