<?php

namespace app\modules\Feedback\components\FeedbackWidget;

use Yii;
use yii\helpers\Html;
use yii\base\Widget;
use app\modules\Feedback\models\Feedback;


class FeedbackWidget extends Widget
{
    
    public $success = false;

    public function run()
    {
        $model = new Feedback();
        
        if (Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->validate())
            {
                $model->save();
                Yii::$app->session->setFlash('feedbackSend', 'Trimis cu success!');
            }
        }
        
        return $this->render('index', [
            'model' => $model
        ]);
    }
    
}