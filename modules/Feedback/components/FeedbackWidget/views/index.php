<?php

    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use yii\widgets\ActiveForm;

?>


<?php Pjax::begin() ?>
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>
        <?= Html::tag('h2', Yii::t('app', 'Mesaj trimis cu success!'), [
            'class' => 'bold'
        ]) ?>
    <?php } else { ?>
    
        <?php $form = ActiveForm::begin([
            'id' => 'feedback-form',
            'options' => [
                'data-pjax' => true,
            ]
        ]); ?>

            <div class="row">
                <div class="col-md-6" data-mh="13">
                    <div>
                        <label>

                            <?= $form->field($model, 'Name')->textInput(['placeholder' =>'Nume / Prenume','class'=>''])->label(false); ?>

                        </label>
                    </div>
                    <div>
                        <label>

                            <?= $form->field($model, 'Email')->textInput(['placeholder' =>'Email','class'=>''])->label(false); ?>

                        </label>
                    </div>
                    <div>
                        <label class="no-margin">
                            <?= $form->field($model, 'Subject')->textInput(['placeholder' =>'Subiect','class'=>''])->label(false); ?>
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div>
                        <label>
                            <?= $form->field($model, 'Message')->textarea(['placeholder' =>'Mesaj','class'=>'','data-mh'=>13])->label(false); ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="text-center">

                <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'TRIMITE') : Yii::t("app", 'TRIMITE'), ['class' => $model->isNewRecord ? 'btn btn-default' : 'btn btn-default',]) ?>

            </div>

        <?php ActiveForm::end(); ?>
    
    <?php } ?>
    
<?php Pjax::end() ?>
    



