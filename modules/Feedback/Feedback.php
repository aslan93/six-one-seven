<?php

namespace app\modules\Feedback;

use app\components\Module\AdminModule;

/**
 * Feedback module definition class
 */
class Feedback extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Feedback\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
