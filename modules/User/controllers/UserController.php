<?php

namespace app\modules\User\controllers;

use app\modules\User\models\UserInfo;
use Yii;
use app\modules\User\models\User;
use app\modules\User\models\UserSearch;
use app\controllers\SiteController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends SiteController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $modelUI = new UserInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $modelUI->UserID = $model->ID;

            if ($modelUI->load(Yii::$app->request->post()))
            {
//                echo '<pre>';
//                print_r($modelUI);
//                echo '</pre>';
//                die();
                $modelUI->save();
                return $this->redirect(['view', 'id' => $model->ID]);
            }
        }
        else
        {
            return $this->render('create', [
                'model' => $model,
                'modelUI' => $modelUI,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelUI = UserInfo::find()->where(['UserID'=> $id])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            if ($modelUI->load(Yii::$app->request->post()) && $modelUI->save())
            {
                return $this->redirect(['view', 'id' => $model->ID]);
            }
        }
        else
        {
            return $this->render('update', [
                'model' => $model,
                'modelUI' => $modelUI,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null)
        {
            return $model;
        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
