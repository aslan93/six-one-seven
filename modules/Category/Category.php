<?php

namespace app\modules\Category;

use app\components\Module\AdminModule;

/**
 * category module definition class
 */
class Category extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Category\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
