<?= $form->field($langModel, "[$langModel->LangID]Title")->textInput() ?>
<?= $form->field($langModel, "[$langModel->LangID]Description")->widget(\dosamigos\tinymce\TinyMce::className(),[
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'theme' => 'modern',
    ]
]) ?>

