<?php

namespace app\modules\Filter\models;

use Yii;
use app\modules\Product\models\Product;
use yii\helpers\ArrayHelper;
use app\modules\Filter\models\PVFImage;

/**
 * This is the model class for table "ProductFilterValue".
 *
 * @property integer $ID
 * @property string $LangID
 * @property integer $ProductID
 * @property integer $FilterID
 * @property string $Value
 *
 * @property Filter $filter
 * @property Product $product
 */
class ProductFilterValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Values = [];
    public static function tableName()
    {
        return 'ProductFilterValue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'ProductID', 'FilterID','Value'], 'required'],
            [['ProductID', 'FilterID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Value'], 'string', 'max' => 255],
            [['Values','UniqID'],'safe'],
            [['FilterID'], 'exist', 'skipOnError' => true, 'targetClass' => Filter::className(), 'targetAttribute' => ['FilterID' => 'ID']],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'ProductID' => 'Product ID',
            'FilterID' => 'Filter ID',
            'Value' => 'Value',
            'Values' => 'Attribute',
            'UniqID' => 'UniqID',
        ];
    }

    public function getFilter()
    {
        return $this->hasOne(Filter::className(), ['ID' => 'FilterID'])->with('lang');
    }
    public function getColor()
    {
        return $this->hasOne(PVFImage::className(), ['PFVID' => 'UniqID']);
    }

    public function getFilterProductValue($lang)
    {
        return ArrayHelper::map([self::find()->where(['UniqID' =>$this->UniqID,'LangID'=>$lang])->one()],'ID','Value');

    }
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID']);
    }

    public function getPFVImage()
    {
        return $this->hasOne(PVFImage::className(), ['PFVID' => 'UniqID']);
    }

    public static  function getList($addEmpty = false,$fid = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $query = self::find()->where(['LangID' => Yii::$app->language])
            ->groupBy('Value')
            ->orderBy('Value');
        if($fid){
            $query->andWhere(['FilterID' => $fid]);
        }
        $result = ArrayHelper::map($query->all(),'Value','Value');
        return $return + $result;
    }
}
