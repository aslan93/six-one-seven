<?php

namespace app\modules\Filter\models;

use Yii;

/**
 * This is the model class for table "FilterLang".
 *
 * @property integer $ID
 * @property string $LangID
 * @property string $Name
 * @property integer $FilterID
 *
 * @property Filter $filter
 */
class FilterLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FilterLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LangID', 'Name', 'FilterID'], 'required'],
            [['FilterID'], 'integer'],
            [['LangID'], 'string', 'max' => 2],
            [['Name'], 'string', 'max' => 255],
            [['FilterID'], 'exist', 'skipOnError' => true, 'targetClass' => Filter::className(), 'targetAttribute' => ['FilterID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'LangID' => 'Lang ID',
            'Name' => 'Name',
            'FilterID' => 'Filter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter()
    {
        return $this->hasOne(Filter::className(), ['ID' => 'FilterID']);
    }
}
