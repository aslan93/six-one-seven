<?php

namespace app\modules\Filter\models;

use Yii;
use app\modules\Filter\models\FilterLang;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "Filter".
 *
 * @property integer $ID
 * @property string $Type
 *
 * @property FilterLang[] $filterLangs
 * @property ProductFilterValue[] $productFilterValues
 */
class Filter extends \yii\db\ActiveRecord
{
    const TypeSelect = 'Select';
    const TypeCheckbox = 'Checkbox';
    const TypeColor = 'Color';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Filter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type'], 'required'],
            [['Type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Type' => 'Type',
        ];
    }


    public function getLangs()
    {
        $langs = $this->hasMany(FilterLang::className(), ['FilterID' => 'ID'])->indexBy('LangID')->all();
        $result = [];
        foreach (array_keys(Yii::$app->params['lang']) as $LangID){
            $result[$LangID]= isset($langs[$LangID]) ? $langs[$LangID] : new FilterLang([
                'LangID' => $LangID,
            ]);
        }
        return $result;
    }

    public function getLang()
    {
        return $this->hasOne(FilterLang::className(), ['FilterID' => 'ID'])->where(['LangID' => Yii::$app->language]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        TagDependency::invalidate(Yii::$app->cache,  self::className());
        $langModels = $this->langs;
        foreach ($langModels as $langModel){

            $langModel->FilterID = $this->ID;
        }

        if(Model::loadMultiple($langModels,Yii::$app->request->post())){

            foreach ($langModels as $langModel){

                $langModel->save();
            }
        }
    }

    public static  function getList($addEmpty = false,$type = false)
    {
        $return = $addEmpty ? ['' => $addEmpty] : [];
        $query = self::find()->with('lang');
        if($type){
            $query->where(['Type' => $type]);
        }
        $result = ArrayHelper::map($query->all(),'ID','lang.Name');
        return $return + $result;
    }

    public static function getTypeList($addEmpty = null){
        $return = $addEmpty ? ['' => $addEmpty] : [];

        $list = [
            self::TypeSelect => Yii::t('app', 'Select'),
            self::TypeCheckbox => Yii::t('app', 'Checkbox'),
            self::TypeColor => Yii::t('app', 'Color'),
        ];
        return $return + $list;
    }

    public function getProductFilterValues()
    {
        return $this->hasMany(ProductFilterValue::className(),['FilterID' => 'ID'])->where(['LangID'=>Yii::$app->language]);
    }

    public function getFilterProductValues($pid,$fid)
    {
        return ProductFilterValue::find()->where(['FilterID' => $fid,'ProductID'=>$pid,'LangID'=> Yii::$app->language])->all();
    }


}
