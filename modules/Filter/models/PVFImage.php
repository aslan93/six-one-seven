<?php

namespace app\modules\Filter\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "PVFImage".
 *
 * @property integer $ID
 * @property string $ColorImage
 * @property string $ProductImage
 * @property integer $PFVID
 *
 * @property ProductFilterValue $pFV
 */
class PVFImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PVFImage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ColorImage', 'ProductImage', 'PFVID'], 'required'],
            [['PFVID'], 'safe'],
            [['ColorImage', 'ProductImage'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ColorImage' => 'Color Image',
            'ProductImage' => 'Product Image',
            'PFVID' => 'Pfvid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPFV()
    {
        return $this->hasOne(ProductFilterValue::className(), ['UniqID' => 'PFVID','LangID'=>yii::$app->language]);
    }
    public function getProductImage()
    {
        return  empty($this->ProductImage) ? '' : Url::to('/uploads/productColors/'.$this->ProductImage);
    }
    public function getColorImage()
    {
        return  empty($this->ColorImage) ? '' : Url::to('/uploads/productColors/'.$this->ColorImage);

    }

}
