<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use app\modules\Filter\models\Filter;

/* @var $this yii\web\View */
/* @var $model app\modules\Filter\models\Filter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filter-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-4">
    <div class="col-md-12">
        <?php
        $items = [];
        foreach ($model->langs as $langID => $langModel)
        {
            $items[] = [
                'label' => strtoupper($langID),
                'content' => $this->render('_desc_form',[
                    'form' => $form,
                    'langModel' => $langModel,
                    'model' => $model,
                ]),
            ];
        }
        echo '<br>';
        echo Tabs::widget([
            'items' => $items,
        ]);
        ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'Type')->dropDownList(Filter::getTypeList('Alege'))->label('Tip') ?>

    </div>
        <div class="form-group col-md-12">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>
