<?php

namespace app\modules\Filter;

use app\components\Module\AdminModule;

/**
 * Filter module definition class
 */
class Filter extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Filter\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
