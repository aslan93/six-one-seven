<?php

namespace app\modules\Ecommerce;

/**
 * Ecommerce module definition class
 */
class Ecommerce extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Ecommerce\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
