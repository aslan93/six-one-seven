<?php

namespace app\modules\Cart\widgets\CartInfo;
use app\modules\Filter\models\ProductFilterValue;

use app\modules\Product\models\Product;
use yii\base\Widget;

class CartInfo extends Widget
{
   public function run()
   {    //unset($_SESSION['cart']);
       $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [] ;
       $count = count($cart);

        $products = [];
        $total = 0;
        foreach ($cart as $key => $item){
           $pfvs = $item['pfv'];
           $pfv = [];
            foreach ($pfvs as $ki =>$i){

               foreach ($i as $k => $pf){

                   $pfv[$ki] = $pf;
               }
           }

            $product = Product::find()->where(['ID'=>$item['ProductID']])->with('lang')->one();

           $image = '';
            if (isset($item['pfv']['Color'])){
                foreach ($item['pfv']['Color'] as $k => $v){
                   $image =  ProductFilterValue::findOne($k)->pFVImage->productImage;
                }
            }else{
                $image = $product->mainImage->imagePath;
            }

           $subtotal = $item['Quantity'] * $product->Price;
           $total += $subtotal;
            $products[] = [
               'Quantity' => $item['Quantity'],
                'title' => $product->lang->Name,
                'image' =>  $image,
                'price' => $product->Price,
                'subtotal' => $subtotal,
                'id' => $product->ID,
                'slug'=> $product->lang->Slug,
                'key' => $key,
                'pfv' => $pfv,

            ];
        }

       return $this->render('index',[
          'products' => $products,
           'total' => $total,
           'count' => $count,
       ]);
   }
}