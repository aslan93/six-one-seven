<?php

use kartik\popover\PopoverX;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\views\themes\shop617\assets\FrontAsset;

$bundle = FrontAsset::register($this);

Pjax::begin([
    'enablePushState'    => false,
    'enableReplaceState' => false,
    'id'                 => 'cart-info-pjax',
    'timeout'            => 5000,
]);
?>

<div class="shop-cart">
    <a href="#">
        COSUL MEU: <?=$count?>
    </a>
    <div class="dropdown-shop-cart hidden-xs ">
        <div class="row">
            <div class="col-md-10 col-sm-10">
                <div class="name-box">
                    <?php
                    if ($count==0){
                        echo 'Nici un produs in cos';
                    }elseif($count == 1){
                        ?>
                        <?=$count?> Produs in cos
                    <?php
                    }else {
                        ?>
                        <?= $count ?> Produse in cos
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="close-shop-cart">
                    <span class="fa fa-close"></span>
                </div>
            </div>
        </div>
        <div class="separator-border"></div>
        <div class="products-added-group <?php if ($count>3){ echo 'scroll-cart';}?>">
            <?php
                foreach ($products as $product) {

            ?>

            <div class="products-added">
                <div class="row">
                    <div class="col-md-1 col-sm-1">
                        <div class="remove-product-cart" data-id="<?=$product['key']?>">
                            <span class="fa fa-close"></span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <a href="<?= Url::to(['/catalog/product', 'slug' => $product['slug']]) ?>" data-pjax="0">
                        <div class="img-product">
                                <?= Html::img($product['image'], ['width' => '100px']) ?>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 text-left">
                        <div class="name-product mb5">
                            <?= $product['title'] ?>
                        </div>
                        <?php
                        foreach ($product['pfv'] as $key => $value){
                        ?>
                            <div class="name-product mb5">
                                <span class="strong-text">
                                    <?=$key?>:
                                </span>
                                <span class="light-text">
                                     <?=$value?>
                                </span>
                            </div>
                            <?php
                        }
                                ?>

                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="quantity-products">
                                    <?= $product['Quantity'] ?> X <br/>
                                    <?= $product['price'] ?> Lei
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
        <div class="separator-border"></div>
        <a href="/cart" class="open-shop-cart-page mt20" data-pjax="0">
            Vezi cosul & Checkout
        </a>
    </div>
</div>

<?php
Pjax::end();
?>

