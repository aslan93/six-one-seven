<?php

namespace app\modules\Cart;

use app\components\Module\FrontModule;

/**
 * Cart module definition class
 */
class Cart extends FrontModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Cart\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
