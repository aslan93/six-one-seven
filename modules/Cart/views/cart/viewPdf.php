<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use app\modules\Order\controllers\QHelper;
use app\views\themes\rowood\assets\RowoodAssets;
use yii\web\View;

?>

<div class="cart-view">

    <style>
        table{
            width: 100%;
            font-family: 'Open Sans';

        }
        table th{
            padding: 10px 0;
        }
        table td{
            text-align: center;
        }
        table .change-width{
            width: 40%;
            text-align: left;
        }
        table td{
            padding: 10px;
        }
        table th.change-width{
            text-align: center;
        }
        .change-width>div>span{
            font-size: 12px;
            font-weight: 700;
        }
    </style>
    <table border="1">
        <thead>
        <tr>
            <th>
                Nr
            </th>
            <th>
                Dnumire
            </th>
            <th>
                Cantitate
            </th>
            <th class="change-width">
                Detalii
            </th>
            <th>
                Pret
            </th>
        </tr>
        </thead>
        <tbody>
        <?foreach ($products as $index => $product) { ?>
            <tr>
                <td>
                    <?=$index+1?>
                </td>
                <td>
                    <span style="font-weight: 900; font-size: 16px"><?=$product['title']?></span>
                </td>
                <td>
                    <?=$product['Quantity']?>
                </td>
                <td class="change-width">
                    <?php

                    foreach ($product['pfv'] as $key => $item) {
                        ?>
                        <div class="name-product">
                                                <span class="strong-text">
                                                    <?= $key ?>:
                                                </span>
                            <span class="light-text">
                                                    <?= $item ?>
                                                </span>
                        </div>
                        <?php
                    }
                    ?>
                </td>
                <td>
                    Unitate: <?=$product['price']?> &euro; <br/>
                    Total: <span style="font-weight: 900; font-size: 16px"><?=$product['subtotal']?> Lei</span>
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td colspan="2">


            </td>
            <td colspan="2">

                Pret total: <span style="font-weight: 900; font-size: 20px;"><?=$total?> Lei</span><br>
            </td>
        </tr>
        </tbody>
    </table>

</div>

<?php
unset($_SESSION['cart']); // resetarea cosului
?>

