<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\Cart\controllers\QHelper;
use yii\web\View;
use app\views\themes\shop617\assets\FrontAsset;


$bundle = FrontAsset::register($this);
?>
<section class="finish-pay">
    <div class="message">
        <div class="big-title">
            Multumim!
        </div>
        <a href="#" class="mt10">
            ACASA
        </a>
    </div>
</section>