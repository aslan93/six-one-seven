<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\Cart\controllers\QHelper;
use yii\web\View;
use app\views\themes\shop617\assets\FrontAsset;


$bundle = FrontAsset::register($this);
?>
<section class="name-page">
    <div class="container">
        <div class="big-title">
            Cos de produse
        </div>
    </div>
</section>
<?php
if (!$products){
    ?>
    <section class="cart">
        <div class="container">
            <div class="row">
                <h1 class="text-center">Nu aveti produse in cos!</h1><br>
                <h3 class="text-center"><a href="/catalog">Mergi la catalog</a></h3><br>

            </div>
        </div>
    </section>
    <?php
}else {
    ?>
    <?php Pjax::begin([
        'id' => 'p1',
    ]) ?>
    <section class="cart">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="cart-box">
                        <div class="header-cart">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-6">
                                    <div class="name-col">
                                        Produs
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    <div class="name-col">
                                        Cant & Pret
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        foreach ($products as $index => $product) { ?>

                            <div class="product">
                                <div class="body-product">
                                    <div class="row">
                                        <?= Html::beginForm('/admin/ecommerce/cart/cart/add-to-cart/', 'get', ['class' => 'quantity-form', 'id' => 'cart-item' . $product['key']]) ?>
                                        <?= Html::hiddenInput('return', true) ?>
                                        <?= Html::hiddenInput('id', $product['id']) ?>
                                        <?= Html::hiddenInput('md5id', $product['key']) ?>
                                        <div class="col-md-8 col-sm-8">
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-12">
                                                    <span class="remove-product fa fa-close"
                                                          data-id="<?= $product['key'] ?>" data-url="<?=Url::to(['/admin/ecommerce/cart/cart/delete-item/?md5id=']) ?>"></span>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <a href="<?= Url::to(['/catalog/product/', 'slug' => $product['slug']]) ?>">
                                                        <?= Html::img($product['image'], ['width' => '100']) ?>
                                                    </a>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <div class="name-product">
                                                        <a href="<?= Url::to(['/catalog/product/', 'id' => $product['id']]) ?>"
                                                           style="text-decoration: none; color: inherit;">
                                                            <?= $product['title'] ?>
                                                        </a>
                                                    </div>
                                                    <?php

                                                    foreach ($product['pfv'] as $key => $item) {
                                                        ?>
                                                        <div class="name-product">
                                                <span class="strong-text">
                                                    <?= $key ?>:
                                                </span>
                                                            <span class="light-text">
                                                    <?= $item ?>
                                                </span>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="input-group">
                                              <span class="input-group-btn">
                                                  <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="quant[<?=$product['key']?>]">
                                                      <span class="fa fa-caret-left"></span>
                                                  </button>
                                              </span>

                                                <?= Html::input('number', 'quantity', $product['Quantity'], [
                                                    'onchange' => "submitForm(this.form)",
                                                    'max'=>100,
                                                    'min'=>1,
                                                    'id' => 'quant['. $product['key'].']',
                                                    'class' => 'form-control input-number',
                                                ]) ?>
                                                <span class="input-group-btn">
                                                  <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="quant[<?=$product['key']?>]">
                                                      <span class="fa fa-caret-right"></span>
                                                  </button>
                                              </span>
                                            </div>



                                            <div class="price">
                                                X <?= $product['price'] ?> Lei
                                            </div>
                                        </div>
                                        <?= Html::endForm() ?>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                        ?>
                        <?php
                        if (Yii::$app->user->isGuest) {

                            ?>
                            <div class="register-now mt20">
                                Client existent?
                                <a href="<?= Url::to(['/login/login/']) ?>">
                                    Da clic aici pentru a te autentifica.
                                </a>
                            </div>
                            <?php
                        }else{
                            ?>
                            <div class="register-now mt20">
                            </div>
                            <?php
                        }
                            ?>
                        <div class="header-cart mt20 mb20">
                            <div class="big-text">
                            </div>
                            Detalii de facturare
                        </div>
                        <form action="#" method="post" id="facture" name="factura">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Nume" name="FirstName" required="required" value="<?=$userInfo->FirstName?>">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Prenume" name="LastName" required="required" value="<?=$userInfo->LastName?>">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Email" name="Email" required="required" value="<?=$userInfo->LastName?>">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Telefon" name="Phone" required="required" value="<?=$userInfo->Phone?>">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Tara" name="Country" required="required" value="<?=$userInfo->Country?>">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Judet" name="Judet" required="required" value="<?=$userInfo->Judet?>">
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Oras" name="City" required="required" value="<?=$userInfo->City?>">
                                    </label>
                                </div>

                                <div class="col-md-6">
                                    <label>
                                        <input type="text" placeholder="Adresa, Numar Apartament(daca este cazul)" name="Address" required="required" value="<?=$userInfo->Address?>">
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3">

                    <div class="right-sidebar">
                        <div class="small-title">
                            TOTAL
                        </div>
                        <form action="#" method="post" id="method" name="method" class="forma-dr">
                        <div >
                            <label>
                                <input type="radio" name="PayType" class="style-radio" value=1>
                                <span class="strong">
                                    Transfer Bancar direct
                                </span> <br/>
                                <span class="light">
                                    Efectuază transferul bancar direct în contul nostru.
                                    Folosește numărul comenzi ca referință de plată.
                                    Comandă nu va fi expediată până nu vom avea confirmarea
                                    transmiterii fondurilor în contul nostru.
                                </span>
                            </label>
                        </div>
                        <div >
                            <label>
                                <input type="radio" name="PayType" class="style-radio" value=2>
                                <span class="strong">
                                    Plata cu card
                                </span> <br/>
                                <span class="light">
                                    Plătește cu card bancar.
                                </span>
                            </label>
                        </div>
                        <div>
                            <label>
                                <input type="radio" name="PayType" class="style-radio" value=3 checked="checked">
                                <span class="strong">
                                    Numerar la livrare
                                </span> <br/>
                                <span class="light">
                                    Plătește cu numerar la livrare.
                                </span>
                            </label>
                        </div>
                        </form>
                        <div class="big-border"></div>
                        <div class="strong-text">
                            <span>
                                TOTAL
                            </span>
                            <span class="pull-right">
                                <?= $total ?> Lei
                            </span>
                        </div>
                        <button class="btn btn-danger submit" id="check-button" onclick="sendThis()">
                            PLASARE COMANDA
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php $this->registerJs(
        "   function submitForm(form){
                var form = $(form);
                var formData = form.serialize();
                $.ajax({
                     url: baseUrl(form.attr('action')),
                     type: form.attr('method'),
                     data: formData,
                    success: function (data) {
                        $.pjax.reload({container: '#p1'});
                    },
                    error: function () {}
                });
            }
            
            function deleteThis(form){
                var form = $(form);
                var formData = form.serialize();
                $.ajax({
                     url: baseUrl('/admin/ecommerce/cart/cart/delete-item'),
                     type: form.attr('method'),
                     data: formData,
                    success: function (data) {
                        $.pjax.reload({container: '#p1'});
                    },
                    error: function () {}
                });
            }
            
        function sendThis(){
        var formData = $('#facture,#method').serialize();
        var errors = 0;
            $('#facture :input').map(function(){
                 if( !$(this).val() ) {
              
                      $(this).parents('label').addClass('warning');
                      errors++;
                } else if ($(this).val()) {
                      $(this).parents('label').removeClass('warning');
                }   
            });
            if(errors > 0){
                alert('Toate cimpurile sunt obligatorii!!!');
                return false;
            }else{
                $.ajax({
                    url: baseUrl('/admin/ecommerce/cart/order/create'),
                    type: 'get',
                    data: formData,
                    success: function (data) {
        
                    },
                    error: function () {}
                });
                }
        };
            "
        , View::POS_HEAD);
    ?>
    <?php
    Pjax::end();
}
?>