<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\modules\Cart\controllers\QHelper;
use yii\web\View;
use app\views\themes\shop617\assets\FrontAsset;


$bundle = FrontAsset::register($this);
?>
<section class="name-page">
    <div class="container">
        <div class="big-title">
            Achitare cu card bancar
        </div>
    </div>
</section>

<section class="transfer">
    <div class="container">
        <div class="transfer-info">
            <div class="panel-box">
                Card bancar
            </div>
            <div class="large-description">
                <div>
                    Credibly underwhelm premium initiatives via impactful "outside the box"
                    thinking. Distinctively integrate functionalized solutions rather
                    than diverse portals. Synergistically maximize sustainable value through
                    transparent vortals. Authoritatively mesh customized products without
                    multifunctional technologies. Phosfluorescently predominate alternative
                    testing procedures for fully tested schemas.
                </div>
                <div>
                    Progressively plagiarize tactical innovation without client-centered
                    customer service. Holisticly actualize extensive quality vectors
                    for turnkey experiences. Progressively communicate end-to-end technologies
                    through multifunctional e-services. Uniquely streamline wireless
                    technologies vis-a-vis backend ROI. Objectively plagiarize ethical
                    innovation for efficient web services.
                </div>
                <div>
                    Professionally streamline inexpensive quality vectors for an expanded
                    array of benefits. Enthusiastically actualize cross functional alignments
                    vis-a-vis diverse resources. Globally disseminate e-business synergy
                    after client-centered applications. Phosfluorescently create wireless
                    interfaces through dynamic communities. Continually repurpose
                    mission-critical alignments rather than high-quality value.
                </div>
                <div>
                    Distinctively restore synergistic leadership skills after standards
                    compliant bandwidth. Completely create an expanded array of outsourcing
                    after orthogonal e-commerce. Credibly unleash interactive deliverables
                    after backend quality vectors. Rapidiously enable high-payoff schemas
                    through standards compliant e-services. Energistically unleash
                    economically sound information with tactical interfaces.
                </div>
                <div>
                    Interactively benchmark client-centered best practices with high-quality
                    niche markets. Globally generate efficient platforms before bricks-and-clicks
                    applications. Intrinsicly strategize installed base e-tailers via 24/365 niches.
                </div>
            </div>
            <div>
                <button type="button" class="btn-default download mt20">
                    <span class="fa fa-file-pdf-o"></span>
                    DESCARCA FACTURA
                </button>
            </div>
        </div>
    </div>
</section>