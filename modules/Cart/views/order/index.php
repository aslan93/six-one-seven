<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use kartik\datetime\DateTimePicker;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Cart\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'UserID',
            'TotalPrice',
            'Status',
            [
                'attribute' => 'OrderDate',
                'label' => 'OrderDate',
                'value' => function ($model){
                    return $model->niceDate;
                },
                'filter'=> DateTimePicker::widget([
                    'model' => $searchModel,
                    'attribute'=> 'OrderDate',
                    'options' => ['placeholder' => 'Select date ...'],
                    'pluginOptions' => [
                        'todayHighlight' => true
                    ]
                ]),

            ],
            // 'PayToken',
            // 'PayType',

            ['class' => 'app\components\GridView\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
