<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\Cart\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
    <div class="col-md-6">

        <div style="margin: 50px; font-size: 20px;">
            <div ><h2>Credentials</h2></div>
            <div class="col-md-12">Nume: <?=$model->orderCredentials->FirstName?></div>
            <div class="col-md-12">Prenume: <?=$model->orderCredentials->LastName?></div>
            <div class="col-md-12">Email: <?=$model->orderCredentials->Email?></div>
            <div class="col-md-12">Phone: <?=$model->orderCredentials->Phone?></div>
            <div class="col-md-12">Country: <?=$model->orderCredentials->Country?></div>
            <div class="col-md-12">Judet: <?=$model->orderCredentials->Judet?></div>
            <div class="col-md-12">City: <?=$model->orderCredentials->City?></div>
            <div class="col-md-12">Address: <?=$model->orderCredentials->Address?></div>
        </div>
    </div>
    <div class="col-md-6">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'UserID')->textInput() ?>

    <?= $form->field($model, 'TotalPrice')->textInput() ?>

    <?= $form->field($model, 'Status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OrderDate')->textInput(['value' => $model->niceDate]) ?>

    <?= $form->field($model, 'PayToken')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PayType')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <br><br>
    <h3>Produse</h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'options' => [
                    'width' => '50px'
                ]
            ],

            [
                'label' => 'Product',
                'value' => function ($model) {
                    return $model->product->lang->Name;
                }
            ],
            [
                'label' => 'Attributes',
                'format' => 'raw',
                'value' => function ($model) {
                   return $model->attributesAsString;
                }
            ],
            [
                'label' => 'Categorie',
                'value' => function ($model) {
                    return $model->product->category->lang->Title;
                }
            ],
            [
                'label' => 'Cantitate',
                'value' => function ($model) {
                    return $model->Quantity .' buc.';
                }
            ],
            [
                'label' => 'Pret/Unitate',
                'value' => function ($model) {
                    return $model->product->Price.' lei';
                }
            ],
            [
                'label' => 'Subtotal',
                'value' => function ($model) {
                    return $model->product->Price * $model->Quantity. ' Lei';
                }
            ],
        ],
    ]); ?>
</div>
