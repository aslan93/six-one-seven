<?php

namespace app\modules\Cart\controllers;

use app\modules\Cart\models\OrderCredentials;
use Yii;
use app\modules\Cart\models\Order;
use app\modules\Cart\models\OrderSearch;
use app\controllers\SiteController;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\Cart\models\OrderProduct;
use app\modules\Product\models\Product;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends SiteController
{


    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
                'query' => OrderProduct::find()->with('product.lang')->where(['OrderProduct.OrderID' => $id]),
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCreate()
    {
        $model = new Order();
        if (Yii::$app->user->identity->ID){
            $model->UserID = Yii::$app->user->identity->ID;
        }
        $total = 0;
        if (isset($_SESSION['cart']))
        {
            if ($model->save()){

                foreach ($_SESSION['cart'] as $item){

                    $serializedPFV = serialize($item['pfv']);
                    $modelProducts = new OrderProduct();
                    $modelProducts->OrderID = $model->ID;
                    $modelProducts->Price = $item['price'];
                    $modelProducts->ProductID = $item['ProductID'];
                    $modelProducts->FilterValues = $serializedPFV;
                    $modelProducts->Quantity = $item['Quantity'];
                    $total += $item['Quantity'] * Product::findOne($item['ProductID'])->Price;
                    if ($modelProducts->save()) {

                    }
                }
            }
        }
        $get = Yii::$app->request->get();
        $model->TotalPrice = $total;
        $model->PayType = $get['PayType'];
        if ($model->save()){}
        if (isset($get)){
            $modelOC = new OrderCredentials();

                $modelOC->OrderID = $model->ID;
                $modelOC->FirstName = $get['FirstName'];
                $modelOC->LastName = $get['LastName'];
                $modelOC->Address = $get['Address'];
                $modelOC->Email = $get['Email'];
                $modelOC->Country = $get['Country'];
                $modelOC->City = $get['City'];
                $modelOC->Phone = $get['Phone'];
                $modelOC->Judet = $get['Judet'];
                $modelOC->save();

        }
        if ($model->PayType==1 || $model->PayType==1){
            Yii::$app->response->redirect('/facture/?orderID='.$model->ID);
            Yii::$app->end();
        }else{
            Yii::$app->response->redirect('/payment/?orderID='.$model->ID);
            Yii::$app->end();
        }

    }


}
