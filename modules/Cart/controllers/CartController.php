<?php

namespace app\modules\Cart\controllers;

use app\modules\Product\models\Product;
use Yii;
use app\controllers\FrontController;
use app\modules\Filter\models\ProductFilterValue;


class CartController extends FrontController
{

    public function actionView($print = false)
    {
        $userInfo =Yii::$app->user->identity->userInfo;
        $cart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [] ;
        $products = [];
        $total = 0;
        foreach ($cart as $key => $item){
            $product = Product::find()->where(['ID'=>$item['ProductID']])->with(['lang'])->one();
            $subtotal = $item['Quantity'] * $product->Price;
            $total += $subtotal;
            $pfv = [];
            $image = '';
            if (isset($item['pfv']['Color'])){
                foreach ($item['pfv']['Color'] as $k => $v){
                    $image =  ProductFilterValue::findOne($k)->pFVImage->productImage;
                }
            }else{
                $image = $product->mainImage->imagePath;
            }

            foreach ($item['pfv'] as $i => $value){
                foreach ($value as $ki =>$val){
                        $pfv[$i] = $val;
                }
            }
            $products[] = [
                'Quantity' => $item['Quantity'],
                'title' => $product->lang->Name,
                'image' => $image,
                'price' => $product->Price,
                'id' => $product->ID,
                'slug' => $product->lang->Slug,
                'subtotal' => $subtotal,
                'key'=>$key,
                'pfv'=>$pfv,
                ];
        }

        if ($print == true) {

             Yii::$app->response->format = 'pdf';
                $this->layout = '//print';
            return $this->renderPartial('viewPdf',[
                'products' => $products,
                'total' => $total,
            ]);
        }else{
            return $this->render('view',[
                'products' => $products,
                'total' => $total,
                'userInfo' => $userInfo,
            ]);
        }
    }

    public function actionAddToCart($id, $quantity = 1, $return = true,$md5id = '')
    {
        if (!isset($_SESSION['cart']))
        {
            $_SESSION['cart'] = [];
        }
        $pfv = [];//product filter values without product id
        foreach (Yii::$app->request->get() as $key => $value) {
            if ($key == 'id' || $key == '_pjax' || $key == 'quantity'){
            }else{
                $model = ProductFilterValue::find()->where(['ID'=>$value,'LangID'=>Yii::$app->language])->one();
                $pfv[$key] = [
                    $value => $model->Value
                ];
            }
        }

        $pfvid = []; //product filter values with product id
        foreach (Yii::$app->request->get() as $key => $value) {
            if ( $key == '_pjax' || $key == 'quantity'){
            }else{
                $pfvid[] = $value;
            }
        }

        $serialized = serialize($pfvid);
        if(!$md5id) {
            $md5id = md5($serialized);
        }
        $product = Product::findOne($id);
        $price = $product->Price;
        
        if (isset($_SESSION['cart'][$md5id]))
        {
            $_SESSION['cart'][$md5id]['Quantity'] = $quantity;
        }
        else
        {
            $_SESSION['cart'][$md5id] = [
                'ProductID' => $id,
                'price' => $price,
                'Quantity' => $quantity,
                'pfv' => $pfv,
            ];
        }

        if ($return)
        {
            //Yii::$app->session->setFlash('success', Yii::t('app', 'Adaugat cu success'));
            return '<div >Adaugat in cos!</div>';
        }
    }


    public function actionDeleteItem($md5id = '')
    {
        if (isset($md5id)){
            $id = $md5id;
        }
        if (isset($_SESSION['cart'][$id]))
        {
            unset($_SESSION['cart'][$id]);
            return 'success';
        }else{
            return false;
        }
    }

    public function actionPdf(){
        Yii::$app->response->format = 'pdf';
        // Rotate the page
        Yii::$container->set(Yii::$app->response->formatters['pdf']['class'], [
            'format' => [216, 356], // Legal page size in mm
            'orientation' => 'Landscape', // This value will be used when 'format' is an array only. Skipped when 'format' is empty or is a string
            'beforeRender' => function($mpdf, $data) {},
        ]);
        //$this->layout = '//print';
        return $this->render('view', []);
    }

    public function actionPayment($orderID)
    {
        return $this->render('payment',[
            'orderID' => $orderID,
        ]);
    }

    public function actionFacture($orderID)
    {
        return $this->render('facture',[
            'orderID' => $orderID,
        ]);
    }

    public function actionFinish()
    {
        return $this->render('finishPay',[]);
    }
}
