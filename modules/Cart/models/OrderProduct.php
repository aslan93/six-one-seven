<?php

namespace app\modules\Cart\models;

use app\modules\Filter\models\ProductFilterValue;
use Yii;
use app\modules\Product\models\Product;
/**
 * This is the model class for table "OrderProduct".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property integer $OrderID
 * @property string $FilterValues
 * @property double $Price
 *
 * @property Order $order
 * @property Product $product
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderProduct';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'OrderID', 'FilterValues', 'Price','Quantity'], 'required'],
            [['ProductID', 'OrderID'], 'integer'],
            [['Price'], 'number'],
            [['FilterValues'], 'string', 'max' => 255],
            [['OrderID'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['OrderID' => 'ID']],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'OrderID' => 'Order ID',
            'FilterValues' => 'Filter Values',
            'Price' => 'Price',
            'Quantity'=>'Quantity',
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['ID' => 'OrderID']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID'])->with('lang');
    }

    public function getAttributesAsString(){
        $attrs = unserialize($this->FilterValues);
        $string = '';
        foreach ($attrs as $key => $attr){
                    foreach ($attr as $a){
                        $string .= $key.': '.$a.'<br>';
                    }


        }
        return $string;
    }

}
