<?php

namespace app\modules\Cart\models;

use Yii;
use app\modules\Product\models\Product;

/**
 * This is the model class for table "Order".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property double $TotalPrice
 * @property string $Status
 * @property string $OrderDate
 * @property string $PayToken
 * @property string $PayType
 *
 * @property OrderProduct[] $orderProducts
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID'], 'integer'],
            [['TotalPrice', 'Status', 'PayToken', 'PayType'], 'safe'],
            [['TotalPrice'], 'number'],
            [['OrderDate'], 'safe'],
            [['Status', 'PayToken', 'PayType'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'TotalPrice' => 'Total Price',
            'Status' => 'Status',
            'OrderDate' => 'Order Date',
            'PayToken' => 'Pay Token',
            'PayType' => 'Pay Type',

        ];
    }

    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['OrderID' => 'ID'])->with('product.lang');
    }

    public function getNiceDate()
    {
        return  date(Yii::$app->params['dateTimeFormatPHP'],strtotime($this->OrderDate));
    }

    public function getOrderCredentials()
    {
        return $this->hasOne(OrderCredentials::className(), ['OrderID' => 'ID']);
    }
}
