<?php

namespace app\modules\Cart\models;

use Yii;

/**
 * This is the model class for table "OrderCredentials".
 *
 * @property integer $ID
 * @property integer $OrderID
 * @property string $FirstName
 * @property string $LastName
 * @property string $Email
 * @property string $Phone
 * @property string $Address
 * @property string $City
 * @property string $Judet
 * @property string $Country
 *
 * @property Order $order
 */
class OrderCredentials extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OrderCredentials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['OrderID', 'FirstName', 'LastName', 'Email', 'Phone', 'Address', 'City', 'Judet', 'Country'], 'required'],
            [['OrderID'], 'integer'],
            [['FirstName', 'LastName', 'Email', 'Phone', 'Address', 'City', 'Judet', 'Country'], 'string', 'max' => 255],
            [['OrderID'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['OrderID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OrderID' => 'Order ID',
            'FirstName' => 'First Name',
            'LastName' => 'Last Name',
            'Email' => 'Email',
            'Phone' => 'Phone',
            'Address' => 'Address',
            'City' => 'City',
            'Judet' => 'Judet',
            'Country' => 'Country',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['ID' => 'OrderID']);
    }
}
