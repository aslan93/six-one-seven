<?php

namespace app\modules\Attribute;

use app\components\Module\AdminModule;

/**
 * Attribute module definition class
 */
class Attribute extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Attribute\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
