<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\file\FileInput;
use \app\modules\Attribute\models\Attribute;

/* @var $this yii\web\View */
/* @var $model app\modules\Attribute\models\Attribute */
/* @var $form yii\widgets\ActiveForm */
$initialPreview = [];
$initialPreviewConfig = [];


    if ($model->image)
    {
        $initialPreview[] = Html::img($model->image, ['width' => 200]);
    }

?>

<div class="attribute-form">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
    ]); ?>

    <div class="col-md-4">
        <?= FileInput::widget([
            'name' => 'Image',
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => [
                'showRemove' => false,
                'showUpload' => false,
                'initialPreview' => $initialPreview,
            ]
        ]) ?>

    </div>

    <div class="col-sm-8">
    <?= $form->field($model, 'Type')->dropDownList(Attribute::getAttrTypeList()) ?>

    <?php
    $items = [];
    foreach ($model->langs as $langID => $langModel)
    {
        $items[] = [
            'label' => strtoupper($langID),
            'content' => $this->render('_desc_form',[
                'form' => $form,
                'langModel' => $langModel,
                'model' => $model,
            ]),
        ];
    }
    echo '<br>';
    echo Tabs::widget([
        'items' => $items,
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
