<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\Attribute\models\Attribute;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Attribute\models\AttributeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attributes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Attribute', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',

            [       'attribute' => 'Type',
                    'filter' =>  Attribute::getAttrTypeList(),

            ],
            'lang.Title',
            [
                'attribute' => 'Imagine',
                'format' => 'raw',
                'value' => function($model)
                {
                    return Html::img($model->image, ['height' => 20]);
                },
                'filter' => false,
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
