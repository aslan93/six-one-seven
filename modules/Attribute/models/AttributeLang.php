<?php

namespace app\modules\Attribute\models;

use Yii;

/**
 * This is the model class for table "AttributeLang".
 *
 * @property integer $ID
 * @property integer $AttributeID
 * @property string $LangID
 * @property string $Title
 * @property string $Description
 *
 * @property Attribute $attribute
 */
class AttributeLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AttributeLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AttributeID', 'LangID', 'Title', 'Description'], 'required'],
            [['AttributeID'], 'integer'],
            [['Description'], 'string'],
            [['LangID'], 'string', 'max' => 2],
            [['Title'], 'string', 'max' => 255],
            [['AttributeID'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::className(), 'targetAttribute' => ['AttributeID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AttributeID' => 'Attribute ID',
            'LangID' => 'Lang ID',
            'Title' => 'Title',
            'Description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribut()
    {
        return $this->hasOne(Attribute::className(), ['ID' => 'AttributeID']);
    }
}
