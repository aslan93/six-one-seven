<?php

namespace app\modules\Attribute\models;

use Yii;
use app\modules\Product\models\Product;
use app\modules\Attribute\models\Attribute;

/**
 * This is the model class for table "ProductAttribute".
 *
 * @property integer $ID
 * @property integer $ProductID
 * @property integer $AttributeID
 * @property string $AttrType
 *
 * @property Product $product
 * @property Attribute $attribute
 */
class ProductAttribute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductAttribute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductID', 'AttributeID', 'AttrType'], 'required'],
            [['ProductID', 'AttributeID'], 'integer'],
            [['AttrType'], 'string', 'max' => 255],
            [['ProductID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['ProductID' => 'ID']],
            [['AttributeID'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::className(), 'targetAttribute' => ['AttributeID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductID' => 'Product ID',
            'AttributeID' => 'Attribute ID',
            'AttrType' => 'Attr Type',
        ];
    }


    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'ProductID'])->with('lang');
    }

    public function getAttribut()
    {
        return $this->hasOne(Attribute::className(), ['ID' => 'AttributeID'])->with('lang');
    }
}
