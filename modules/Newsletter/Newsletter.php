<?php

namespace app\modules\Newsletter;

use app\components\Module\AdminModule;
/**
 * Newsletter module definition class
 */
class Newsletter extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Newsletter\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
