<?php

namespace app\modules\Newsletter\models;

use Yii;
/**
 * This is the model class for table "Newsletter".
 *
 * @property integer $ID
 * @property string $Email
 */
class Newsletter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Newsletter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Email'], 'required'],
            [['Email'], 'email'],
            [['Email'], 'unique'],
            [['Email'], 'string', 'max' => 255],
            [['Date'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Email' => Yii::t('app', 'Email'),
            'Date' => Yii::t('app', 'Date'),
        ];
    }
}
