<?php

use yii\helpers\Html;
//echo '<pre>';
//print_r($order);
?>

<a href="<?=Yii::$app->request->referrer?>" style="background-color: #ff5000;
    color: black;
    padding: 5px;
    border-radius: 5px;
    display: block;
    margin-bottom: 5px;
    width: 70px;
    text-decoration: none;">
    <i class="fa fa-arrow-left"></i> Back</a>
<div class="statistic mb30">
    Comanda <span class="strong">#<?=$order->ID?></span> a fost plasată pe <span class="strong"><?=$order->niceDate?></span> și acum este <?=$order->Status?>.
</div>
<div class="panel-box mt20">
    PRODUSE
</div>
<div class="scroll-table">
    <table class="no-border width-auto">
        <tr>
            <th>
                Produs
            </th>
            <th>
                Pret
            </th>
            <th>
                Atribute
            </th>
            <th>
                Cantitate
            </th>
            <th>
                Total
            </th>
        </tr>
        <?php
        foreach ($order->orderProducts as $product){
            ?>
            <tr>
                <td>
                <?=$product->product->lang->Name?>
                </td>
                <td>
                    <?=$product->Price?> L
                </td>
                <td>
                    <?=$product->attributesAsString?>
                </td>
                <td>
                    <?=$product->Quantity ?> buc.
                </td>
                <td>
                    <?=$product->Price * $product->Quantity?> L
                </td>

            </tr>
            <?php
        }
        ?>

    </table>

</div>
<div class="panel-box mt20">
    DETALI CLIENT
</div>
<div class="scoll-table">
    <table>
        <tr>
            <td>
                Email:
            </td>
            <td>
                <?=$order->orderCredentials->Email?>
            </td>
        </tr>
        <tr>
            <td>
                Telefon:
            </td>
            <td>
                <?=$order->orderCredentials->Phone?>
            </td>
        </tr>
    </table>
</div>
<div class="panel-box mt20">
    ADRESA DE FACTURARE
</div>
<div class="scroll-table">
    <table class="egal-width-td">
        <tr>
            <td class="strong">
                <?=$order->orderCredentials->FirstName?> <?=$order->orderCredentials->LastName?>
            </td>
            <td>
                <?=$order->orderCredentials->Country?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$order->orderCredentials->Judet?>
            </td>
            <td>
                <?=$order->orderCredentials->City?>
            </td>
        </tr>
        <tr>
            <td>
                <?=$order->orderCredentials->Address?>
            </td>
            <td>
                <?=$order->orderCredentials->Phone?>
            </td>
        </tr>
    </table>
</div>
