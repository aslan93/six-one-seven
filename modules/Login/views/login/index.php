<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\User\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = Yii::t('app', 'Logare');
?>


<section class="name-page">
    <div class="container">
        <div class="big-title">
            Logare
        </div>
    </div>
</section>

<section class="login-user">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="login-box">
                    <div class="panel-box">
                        Logare
                    </div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{input}",
                            'options' => [
                                'tag' => false,
                            ],
                        ],
                    ]); ?>
                        <div>
                            <label>
                                <?= $form->field($model, 'Email')->textInput(['autofocus' => false,'class'=>''])->label(false) ?>
                            </label>
                        </div>
                        <div>
                            <label>
                                <?= $form->field($model, 'Password')->passwordInput(['class'=>''])->label(false) ?>
                            </label>
                        </div>
                         <?= $form->field($model, 'RememberMe')->checkbox() ?>
                        <div class="text-left">
                            <?= Html::submitButton(Yii::t('app', 'Logare'), ['class' => 'btn btn-danger submit']) ?>
                        </div>



                    <?php ActiveForm::end(); ?>

                </div>
            </div>
            <div class="col-md-6">
                <div class="register-box">
                    <div class="panel-box">
                        Inregistrare
                    </div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'register-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{input}",
                            'options' => [
                                'tag' => false,
                            ],
                        ],
                    ]); ?>

                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'FirstName')->textInput(['maxlength' => true, 'class'=>'','placeholder'=>'Nume']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'LastName')->textInput(['maxlength' => true ,'class'=>'','placeholder'=>'Prenume']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'Sex')->widget(Select2::className(),[
                                'data' => \app\modules\User\models\UserInfo::getSexType(),
                                'class' => 'style-select',
                                'hideSearch'=> true,

                            ])->label(false)?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelREG, 'Email')->textInput(['autofocus' => true ,'class'=>'','placeholder'=>'Email']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelREG, 'Password')->passwordInput(['class'=>'','placeholder'=>'Password']) ?>

                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'Phone')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Telefon']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'Country')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Tara']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'Judet')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Judet']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'City')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Oras']) ?>
                        </label>
                    </div>
                    <div>
                        <label>
                            <?= $form->field($modelRUI, 'Address')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Adresa']) ?>
                        </label>
                    </div>

                    <div class="text-left">
                        <?= Html::submitButton(Yii::t('app', 'Inregistrare'), ['class' => 'btn btn-danger submit']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>