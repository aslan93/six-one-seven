<?php

use app\views\themes\shop617\assets\FrontAsset;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use kartik\select2\Select2;

$bundle = FrontAsset::register($this);


?>
<section class="name-page">
    <div class="container">
        <div class="big-title">
            Contul meu
        </div>
    </div>
</section>

<section class="user-account">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="user-account-tabs">
                    <div class="small-title">
                        Panou de control
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#panou-de-control" aria-controls="panou-de-control" role="tab" data-toggle="tab">
                                PANOU DE CONTROL
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#comenzi" aria-controls="comenzi" role="tab" data-toggle="tab">
                                COMENZI
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#adrese" aria-controls="adrese" role="tab" data-toggle="tab">
                                ADRESE
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#detalii-cont" aria-controls="detalii-cont" role="tab" data-toggle="tab">
                                DETALII CONT
                            </a>
                        </li>
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['/login/login/logout'])?>" class="btn btn-default" style="background: #1d1e23; color: white;">
                                DEAUTENTIFICARE
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="panou-de-control">
                        <div class="panel-box mt10">
                            Panou de Control
                        </div>
                        <div class="text">
                            <div>
                                Bună <span class="strong"><?=$user->userInfo->FirstName?> <?=$user->userInfo->LastName?></span> (nu ești <span class="strong"><?=$user->userInfo->FirstName?> <?=$user->userInfo->LastName?></span>? <a href="/login/login/logout">Deconectează-te</a>)
                            </div>
                            <div>
                                Din panoul de control al contului tău poți să-ți vizualizezi
                                <a href="#">comenzile recente</a>, să-ți
                                <a href="#">administrezi adresele de livrare și facturare</a> și să-ți
                                <a href="#">editezi parola și detaliile contului.</a>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="comenzi">
                        <?php
                        \yii\widgets\Pjax::begin()
                        ?>

                            <div class="panel-box mt10 mb40">
                                DETALII COMENZI
                            </div>
                            <div class="scroll-table">
                                <table class="no-border width-auto">
                                    <tr>
                                        <th>
                                            Comanda
                                        </th>
                                        <th>
                                            Data
                                        </th>
                                        <th>
                                            Stare
                                        </th>
                                        <th>
                                            Total
                                        </th>
                                        <th>
                                            &nbsp;
                                        </th>
                                    </tr>
                                    <?php
                                    foreach ($orders as $order) {
                                        ?>
                                        <tr>
                                            <td>
                                                #<?= $order->ID ?>
                                            </td>
                                            <td>
                                                <?= $order->niceDate ?>
                                            </td>
                                            <td>
                                                <?= $order->Status ?>
                                            </td>
                                            <td>
                                                <?= $order->TotalPrice ?>L
                                            </td>
                                            <td>
                                                <a href="<?= \yii\helpers\Url::to(['/login/login/order', 'id' => $order->ID]) ?>"
                                                   class="btn btn-danger submit" data-pjax="1">
                                                    VIZUALIZARE
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                </table>
                                <?php
                                \yii\widgets\Pjax::end();
                                ?>
                            </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="adrese">
                        <div class="panel-box">
                            Adrese
                        </div>
                        <?php
                        \yii\widgets\Pjax::begin()
                        ?>
                        <?php $form = ActiveForm::begin([
                            'action' => \yii\helpers\Url::to(['/login/login/view','uid'=>$user->ID]),
                            'id' => 'form2',
                            'options' => ['data-pjax' => ''],
                            'fieldConfig' => [
                                'template' => "{input}",
                                'options' => [
                                    'tag' => false,
                                ],
                            ],

                        ]); ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'FirstName')->textInput(['maxlength' => true, 'class'=>'','placeholder'=>'Nume']) ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'LastName')->textInput(['maxlength' => true ,'class'=>'','placeholder'=>'Prenume']) ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'Sex')->widget(Select2::className(),[
                                            'data' => \app\modules\User\models\UserInfo::getSexType(),
                                            'class' => 'style-select',
                                            'hideSearch'=> true,

                                        ])->label(false)?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'Phone')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Telefon']) ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'Country')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Tara']) ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'Judet')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Judet']) ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'City')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Oras']) ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <?= $form->field($modelRUI, 'Address')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Adresa']) ?>
                                    </label>
                                </div>
                            </div>
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Salveaza'), ['class' => 'btn btn-danger submit']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                        <?php
                        \yii\widgets\Pjax::end()
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="detalii-cont">
                        <div class="panel-box">
                            Detalii cont
                        </div>
                        <?php
                        \yii\widgets\Pjax::begin()
                        ?>
                        <?php $form = ActiveForm::begin([
                            'action' => \yii\helpers\Url::to(['/login/login/view','uid'=>$user->ID]),
                            'id' => 'form1',
                            'options' => ['data-pjax' => '' ],
                            'fieldConfig' => [
                                'template' => "{input}",
                                'options' => [
                                    'tag' => false,
                                ],
                            ],
                        ]); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>
                                        <?= $form->field($user, 'Email')->textInput(['maxlength' => true,'class'=>'','placeholder'=>'Email']) ?>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label>
                                        <?= $form->field($user, 'Password')->passwordInput(['class'=>'','placeholder'=>'Password']) ?>
                                    </label>
                                </div>
                            </div>
                            <div class="text-right">
                                <?= Html::submitButton(Yii::t('app', 'Salveaza'), ['class' => 'btn btn-danger submit']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                        <?php
                        \yii\widgets\Pjax::end()
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>