<?php

namespace app\modules\Login\controllers;

use app\modules\Cart\models\Order;
use app\modules\User\models\UserInfo;
use Yii;
use app\controllers\FrontController;
use app\modules\Login\models\LoginForm;
use yii\helpers\Url;
use app\modules\User\models\User;

class LoginController extends FrontController
{

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->loginUrl = ['/login/login/view', 'return' => Yii::$app->request->url];
            return $this->redirect(Yii::$app->user->loginUrl);
        }else {

            $model = new LoginForm();

            if ($model->load(Yii::$app->request->post()) && $model->login()) {

                if (Yii::$app->user->isGuest) {
                    Yii::$app->user->loginUrl = ['/login', 'return' => Yii::$app->request->url];
                    return $this->redirect(Yii::$app->user->loginUrl);
                }

                if (Yii::$app->user->identity->Type == 'Admin') {
                    return $this->redirect(Url::to(['/admin/']));

                } else {

                    return $this->redirect(Url::to(['/login/login/view']));
                }
            }

            //save user
            $modelREG = new User();
            $modelRUI = new UserInfo();
            $modelREG->Type = 'User';
            $modelREG->Status = 'Active';
            if ($modelREG->load(Yii::$app->request->post()) && $modelREG->save()) {

                //save user info
                $modelRUI->load(Yii::$app->request->post());
                $modelRUI->UserID = $modelREG->ID;
                $modelRUI->save();

                //LOGIN
                $model->Email = $modelREG->Email;
                $model->Password = Yii::$app->request->post('User')['Password'];
                $model->RememberMe = 1;
                if ($model->login()) {
                    return $this->redirect(Url::to(['/login/login/view/']));
                } else {
                    Yii::$app->user->loginUrl = ['/login/login/index/'];
                    return $this->redirect(Yii::$app->user->loginUrl);
                }

            }

            return $this->render('index', [
                'model' => $model,
                'modelREG' => $modelREG,
                'modelRUI' => $modelRUI,
            ]);
        }
    }

    public function actionView()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->user->loginUrl = ['/login/login/index'];
            return $this->redirect(Yii::$app->user->loginUrl);
        }else {

            $user = Yii::$app->user->identity;
            if (!$user->userInfo){
                $userInfo = new UserInfo();
            }else{
                $userInfo = $user->userInfo;
            }


            if (Yii::$app->request->isPost) {
                if ($user->Password == Yii::$app->request->post('User')['Password']) {
                    unset($_POST['User']['Password']);
                }
                $userInfo->UserID = $user->ID;
                if ($user->load($_POST) && $user->save()) {}
                elseif ($userInfo->load(Yii::$app->request->post()) && $userInfo->save()) {
                }
            }

            return $this->render('account', [
                'user' => $user,
                'orders' => $user->userOrders,
                'modelRUI' => $userInfo,
            ]);
        }
    }

    public function actionOrder($id)
    {
            return $this->renderAjax('order', [
                'order' => Order::find()->where(['ID'=>$id])->with('orderProducts','orderCredentials')->one(),
            ]);
    }

    public function actionUpdate($uid)
    {
        $user = User::find()->where(['ID'=>$uid])->with('userInfo')->one();


    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
