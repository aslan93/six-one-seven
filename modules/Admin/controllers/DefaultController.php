<?php

namespace app\modules\Admin\controllers;

use app\controllers\SiteController;
use Yii;

/**
 * Default controller for the `Admin` module
 */
class DefaultController extends SiteController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function init()
    {
        parent::init();

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}
