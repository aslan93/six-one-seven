<?php

namespace app\modules\Admin;

use app\components\Module\AdminModule;

/**
 * Admin module definition class
 */
class Admin extends AdminModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
